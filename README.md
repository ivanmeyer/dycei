# README #

Este README indicará los lineamientos definidos para el desarrollo y verificación del software.

### Objetivo ###

Repositorio para el desarrollo del software para **Detección y Caracterización de Eventos de Incendio mediante Información Satelital**

### Utilización ###

* #### Branch ####

    Los branches o ramas deberán tener el siguiente formato:
    
    *E[N°_Entregable]-[Inicial_Nombre]-[ID_y_Nombre_Tarea]*
    
    Se crearán los branches que sean necesarios. Si se empieza a programar un grupo de tareas que requiera realmente pocos commits podrá realizarse dentro del grupo de trabajo sin crear nuevos branches.
    
    Ejemplos:
    
    |     Nombre de branch        |          Significado                                            |
    |:----------------------------| :---------------------------------------------------------------|
    |*Master*                     |                                                                 | 
    |. *E1*                       |Entregable 1                                                     |
    |. . *E1-21_Focos*            |Empezamos el grupo 2.1 Focos. *Requiere pocos Commits*           |
    |. . *E1-22_Vector*           |Idem grupo 2.2 Vector. *Político y Ciudades no sería muy extenso*|
    |. . . *E1-I-223_AreasProte*  |Iván con tarea 2.2.3 Áreas Protegidas. *Tarea de varios Commits* |
    |. . . *E1-D-224_Rutas*       |Daniel con tarea 2.2.4 Rutas. *Tarea de varios Commits*          |
    |. . *E1-23_Rasters*          |Empezamos el grupo 2.3 Raster                                    |
    |. . . *E1-I-231_GCL2000*     |Iván empezó la tarea 2.3.1 GCL2000                               |
    |. . . *...otros...*          |                                                                 |
    |. *E2*                       |Entregable 2                                                     |
    |. . *E2-33_ZonasAfect*       |Empezamos el grupo 3.3 Delimitar zonas afectadas                 |
    |. . . *E2-D-331_SupIncendio* |Daniel con tarea 3.3.1 Superficie de Incendio                    |



* #### GIT ####

* ###### Uso general ######

    Traer los cambios de un branch del repositorio remoto 
    
        git pull origin nombre_branch
    
    
    Subir los cambios de un branch al repositorio remoto
    
        git push origin nombre_branch
    
    
    Traer un branch remoto que no está en nuestra máquina
    
        git fetch origin nombre_branch_remoto:repetir_nombre_branch_remoto
    
    
    Actualizar el branch de *mi_tarea* con las modificaciones realizadas en el branch del entregable *Ex*. (Fusionar los cambios del entregable hacia mi branch actual)
    
        git checkout branch_Ex_tarea_actual
        git rebase branch_Ex

    
    Agregar al branch del entregable *Ex*, el branch de mi tarea terminada y testeada. (Fusionar los cambios de mi branch actual hacia el entregable)

        git checkout branch_Ex
        # Verificar que el branch_Ex esté actualizado con git status...
        (git pull origin branch_Ex)
        # Fusión de ramas:
        git merge branch_Ex_tarea_terminada
        # Testear cambios. Realizar adaptaciones necesarias...
        (git push origin branch_Ex)
    

    Ver el historial de commits del branch actual, con sus comentarios

        git log


* ###### Ver modificaciones ######

    Ver las modificaciones de los archivos NO "adicionados" (mediante `git add`) respecto del último commit. Usar *enter* para avanzar por línea y *espacio* para avanzar por página.

        git diff


    Ver las modificaciones de los archivos "adicionados" (mediante `git add`) respecto del último commit. Usar *enter* para avanzar por línea y *espacio* para avanzar por página.

        git diff --cached


    Ver las modificaciones de los archivos respecto del último commit. Usar *enter* para avanzar por línea y *espacio* para avanzar por página.

        git diff HEAD


    Ver las modificaciones de los archivos respecto del último commit mediante interfaz gráfica

        git difftool

* ###### Revertir operaciones ######

    Buscar última operación correcta y revertir a esa instancia

        # Busco la última operación correcta (copio el nombre del commit)
        git reflog
        # Revierto al commit deseado
        git reset --hard nombre_commit

* Otros comandos importantes se encuentran en [este link](https://drive.google.com/folderview?id=0B-SFkH9p6rSVfm9BQTFTREF1LVpWRjkxZXpNaHkyalZ0RlJ5QkJYaFlmdUdHQjl0OXdfTHM&usp=drive_web).


### Contactos ###

* Ivan Meyer .......... ivan.meyer00@gmail.com
* Daniel Martelliti ... danielmartelliti@yahoo.com.ar