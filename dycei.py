    #!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Nombre de script:    index.py
Objetivo:            Ejecutable inicial de nuestro programa
Autor:               Daniel e Iván
"""

import os, sys

from Clases.foco_y_evento       import Evento
from Clases.familia_raster      import *
from Modulos.graficador         import *
from Modulos.detector_eventos   import DetEventos
from Modulos.decode_focos       import Decoder, Adjuntar_Rutas_Deptos
from Modulos.funciones          import Norma,ToFile
from Modulos.manejo_shp         import Comparar_Entradas_Shp, Adjuntar_Localidad
from Modulos.definiciones       import *
from PyQt4                      import QtGui,QtCore




pathActual = os.getcwd()



###############################################################################

class Configuracion(QtGui.QDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u"Configuración")
        self.setWindowIcon(QtGui.QIcon(dir_iconos+'fuego.png'))

#        if os.path.exists(self.ubicacion_raster):
#            self.line_edit_ubic.setText(str(self.ubicacion_raster))

        self.dir_mod11 = dir_mod11
        self.dir_mod13 = dir_mod13
        self.dir_glc2000   = dir_glc2000

###############################################################################
        fila_actual = 1
        fila_inicial = 20
        columna_2 = 20
        offset_col = 20


        fila_actual += 1
        Texto_analisis_event1 = QtGui.QLabel(u'Ubicación MOD11:',self)
        Texto_analisis_event1.move(columna_2, fila_inicial + 20*fila_actual)
        Texto_analisis_event1.resize(Texto_analisis_event1.sizeHint())
        Texto_analisis_event1.setEnabled(False)

###############################################################################

        fila_actual += 1
        Widget_ubicacion1 = QtGui.QWidget(self)
        Widget_ubicacion1.move(columna_2 + offset_col, fila_inicial + 20*fila_actual)
        Widget_ubicacion1.resize(350,30)
        self.line_edit_ubic1 = QtGui.QLineEdit(Widget_ubicacion1)
        self.line_edit_ubic1.resize(300,20)
        self.line_edit_ubic1.move(0, 0)
        self.line_edit_ubic1.setText(str(self.dir_mod11))
        #self.cBox_raster.append(self.line_edit_ubic)
        botonUbicEvento1 = QtGui.QPushButton('...', Widget_ubicacion1)
        botonUbicEvento1.resize(25,20)
        botonUbicEvento1.move(310,0)
        botonUbicEvento1.clicked.connect(self.Ubicacion_Mod11)
        #Widget_ubicacion.setEnabled(False)
        #self.cBox_raster.append(Widget_ubicacion)

###############################################################################

        fila_actual += 3
        Texto_analisis_event2 = QtGui.QLabel(u'Ubicación MOD13:',self)
        Texto_analisis_event2.move(columna_2, fila_inicial + 20*fila_actual)
        Texto_analisis_event2.resize(Texto_analisis_event2.sizeHint())
        Texto_analisis_event2.setEnabled(False)

###############################################################################

        fila_actual += 1
        Widget_ubicacion2 = QtGui.QWidget(self)
        Widget_ubicacion2.move(columna_2 + offset_col, fila_inicial + 20*fila_actual)
        Widget_ubicacion2.resize(350,30)
        self.line_edit_ubic2 = QtGui.QLineEdit(Widget_ubicacion2)
        self.line_edit_ubic2.resize(300,20)
        self.line_edit_ubic2.move(0, 0)
        self.line_edit_ubic2.setText(str(self.dir_mod13))
        #self.cBox_raster.append(self.line_edit_ubic)
        botonUbicEvento2 = QtGui.QPushButton('...', Widget_ubicacion2)
        botonUbicEvento2.resize(25,20)
        botonUbicEvento2.move(310,0)
        botonUbicEvento2.clicked.connect(self.Ubicacion_Mod13)
        #Widget_ubicacion.setEnabled(False)
        #self.cBox_raster.append(Widget_ubicacion)

###############################################################################

        fila_actual += 3
        Texto_analisis_event3 = QtGui.QLabel(u'Ubicación GLC2000:',self)
        Texto_analisis_event3.move(columna_2, fila_inicial + 20*fila_actual)
        Texto_analisis_event3.resize(Texto_analisis_event3.sizeHint())
        Texto_analisis_event3.setEnabled(False)

###############################################################################

        fila_actual += 1
        Widget_ubicacion3 = QtGui.QWidget(self)
        Widget_ubicacion3.move(columna_2 + offset_col, fila_inicial + 20*fila_actual)
        Widget_ubicacion3.resize(350,30)
        self.line_edit_ubic3 = QtGui.QLineEdit(Widget_ubicacion3)
        self.line_edit_ubic3.resize(300,20)
        self.line_edit_ubic3.move(0, 0)
        self.line_edit_ubic3.setText(str(self.dir_glc2000))
        #self.cBox_raster.append(self.line_edit_ubic)
        botonUbicEvento3 = QtGui.QPushButton('...', Widget_ubicacion3)
        botonUbicEvento3.resize(25,20)
        botonUbicEvento3.move(310,0)
        botonUbicEvento3.clicked.connect(self.Ubicacion_Glc2000)


    def Ubicacion_Mod11(self):
        self.dir_mod11 = str(QtGui.QFileDialog.getExistingDirectory(self, 'Selecionar directorio', \
            pathActual)) + slash


        if os.path.exists(self.dir_mod11):
            self.line_edit_ubic1.setText(str(self.dir_mod11))
        else:
            print "Problema el fichero no existe"
            return

    def Ubicacion_Mod13(self):
        self.dir_mod13 = str(QtGui.QFileDialog.getExistingDirectory(self, 'Selecionar directorio', \
            pathActual)) + slash


        if os.path.exists(self.dir_mod13):
            self.line_edit_ubic2.setText(str(self.dir_mod13))
        else:
            print "Problema el fichero no existe"
            return

    def Ubicacion_Glc2000(self):
        self.dir_glc2000 = str(QtGui.QFileDialog.getExistingDirectory(self, 'Selecionar directorio', \
            pathActual)) + slash


        if os.path.exists(self.dir_glc2000):
            self.line_edit_ubic3.setText(str(self.dir_glc2000))
        else:
            print "Problema el fichero no existe"
            return

###############################################################################

class Licencia(QtGui.QDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u"Licencia")
        self.setWindowIcon(QtGui.QIcon(dir_iconos+'fuego.png'))

#        if os.path.exists(self.ubicacion_raster):
#            self.line_edit_ubic.setText(str(self.ubicacion_raster))


###############################################################################
        fila_actual = 1
        fila_inicial = 20
        columna_2 = 20
        columna_3 = 50



        fila_actual += 1
        Texto_licencia = QtGui.QLabel(txt_licencia,self)

        Texto_licencia.move(columna_2, fila_inicial + 20*fila_actual)
        Texto_licencia.resize(Texto_licencia.sizeHint())
#        Texto_licencia.setEnabled(False)


        fila_actual += 8
        Texto_autores = QtGui.QLabel(txt_autores,self)
        font = QtGui.QFont()
        font.setBold(True)
        Texto_autores.setFont(font)

        Texto_autores.move(columna_3, fila_inicial + 20*fila_actual)
        Texto_autores.resize(Texto_autores.sizeHint())
#        Texto_licencia.setEnabled(False)

###############################################################################



class Interface_Grafica(QtGui.QMainWindow):

###############################################################################
##  Inicializacion de los objetos creados de la clase instanciada
    def __init__(self):
        super(Interface_Grafica, self).__init__()

    ##  Defino diversos atributos que se utilizaran en el programa
        self._testeo = False
        self.ubicacion_raster = ""
        self.estado = 0
        self.cBox_analisis_raster = []
        self.ventana = Configuracion()
        self.licencia = Licencia()
        self.initUI()




    def Notificar(self, mensaje):
        msg = QtGui.QMessageBox()
        modo = mensaje.split(',')[0]
        if modo == "Problema":
            msg.setIcon(QtGui.QMessageBox.Warning)
        else:
            msg.setIcon(QtGui.QMessageBox.Information)
        msg.setWindowTitle('DyCEI')
        msg.setWindowIcon(QtGui.QIcon(dir_iconos+'fuego.png'))
        msg.setButtonText(1,"&Aceptar")
        msg.setText(mensaje.decode('utf-8'))
        msg.exec_()

###############################################################################
###############################################################################
##  Inicio la distribucion de los objetos sobre la interfaz grafica
    def initUI(self):

        self.statusBar()

        openFile = QtGui.QAction(QtGui.QIcon(dir_iconos+'open.png'), 'Abrir', self)
        openFile.setShortcut('Ctrl+O')
        openFile.setStatusTip('Cargar archivo fuente')
        openFile.triggered.connect(self.Open_and_Decode)

        self.Texto_inicio = QtGui.QLabel(txt_inicio,self)
        self.Texto_inicio.move(300 , 10)
        self.Texto_inicio.resize(self.Texto_inicio.sizeHint())
        self.Texto_inicio.setStatusTip("txt")


        fila_inicial = 40
        fila_actual = 0
        columna_1 = 20
        offset_col = 20


#########################  FASE 1: FILTRADO DE DATOS  #########################

        self.cBox_provincias = []
        self.Texto_fase_1 = QtGui.QLabel('Fase 1: Filtrado de datos',self)
        self.Texto_fase_1.move(columna_1 , fila_inicial + 20*fila_actual)
        self.Texto_fase_1.resize(self.Texto_fase_1.sizeHint())
        self.Texto_fase_1.setToolTip(str_help["F1: titulo"].decode('utf-8'))
        self.Texto_fase_1.setEnabled(False)
        self.cBox_provincias.append(self.Texto_fase_1)

###############################################################################

        fila_actual += 1
        self.Texto_fase_1 = QtGui.QLabel('Filtro espacial: provincias.',self)
        self.Texto_fase_1.move(columna_1 , fila_inicial + 20*fila_actual)
        self.Texto_fase_1.resize(self.Texto_fase_1.sizeHint())
        self.Texto_fase_1.setToolTip(str_help["F1: provincias"].decode('utf-8'))
        self.Texto_fase_1.setEnabled(False)
        self.cBox_provincias.append(self.Texto_fase_1)

###############################################################################
        fila_actual += 1
        for i in range(len(provincias)):
            checkBoxAux = QtGui.QCheckBox(provincias[i], self)
            checkBoxAux.move(columna_1 + offset_col +(i%2)*180, fila_inicial + 20*(int(i/2)+fila_actual))
            checkBoxAux.resize(checkBoxAux.sizeHint())
            checkBoxAux.setEnabled(False)
            self.cBox_provincias.append(checkBoxAux)

###############################################################################

        fila_actual += len(provincias)/2 +2
        self.Texto_fase_1 = QtGui.QLabel('Filtro temporal: Fechas.',self)
        self.Texto_fase_1.move(columna_1 , fila_inicial + 20*fila_actual)
        self.Texto_fase_1.resize(self.Texto_fase_1.sizeHint())
        self.Texto_fase_1.setToolTip(str_help["F1: fechas"].decode('utf-8'))
        self.Texto_fase_1.setEnabled(False)
        self.cBox_provincias.append(self.Texto_fase_1)

###############################################################################
        fila_actual += 1

        Widget_fecha = QtGui.QWidget(self)
        Widget_fecha.resize(350,50)
        Widget_fecha.move(columna_1 + offset_col, fila_inicial + 20*fila_actual)

        Texto_fecha_inicial = QtGui.QLabel('Fecha inicial:',Widget_fecha)
        Texto_fecha_inicial.move(0 , 0)
        Texto_fecha_inicial.resize(Texto_fecha_inicial.sizeHint())

        self.fecha_inicial = QtGui.QDateEdit(Widget_fecha)
        self.fecha_inicial.setDisplayFormat("dd/MM/yyyy")
        self.fecha_inicial.setDate(QtCore.QDate.currentDate())
        self.fecha_inicial.resize(self.fecha_inicial.sizeHint())
        self.fecha_inicial.move(0, 25)

        Texto_fecha_final =   QtGui.QLabel('Fecha final:', Widget_fecha)
        Texto_fecha_final.move(160 ,0)
        Texto_fecha_final.resize(Texto_fecha_final.sizeHint())

        self.fecha_final = QtGui.QDateEdit(Widget_fecha)
        self.fecha_final.setDisplayFormat("dd/MM/yyyy")
        self.fecha_final.setDate(QtCore.QDate.currentDate())
        self.fecha_final.resize(self.fecha_final.sizeHint())
        self.fecha_final.move(160,25)

        Widget_fecha.setEnabled(False)
        self.cBox_provincias.append(Widget_fecha)

###############################################################################

    ##  Seteo las propiedades del Boton
        # Crea un boton en el Widget
        fila_actual += 3
        botonFiltrar = QtGui.QPushButton('Filtrar Datos', self)
        botonFiltrar.resize(botonFiltrar.sizeHint())
        botonFiltrar.move(columna_1 + offset_col, fila_inicial + 20*fila_actual)
        botonFiltrar.clicked.connect(self.Filtrar_datos)
        botonFiltrar.setToolTip(str_help["F1: boton filtrar"].decode('utf-8'))
        botonFiltrar.setEnabled(False)
        self.cBox_provincias.append(botonFiltrar)

###############################################################################

        self.cBox_Acciones = []
        botonPloteoGerenal = QtGui.QPushButton('Grafico General', self)
        botonPloteoGerenal.resize(botonPloteoGerenal.sizeHint())
        botonPloteoGerenal.move(columna_1 + offset_col*9, fila_inicial + 20*fila_actual)
        botonPloteoGerenal.clicked.connect(self.Ploteo_General)
        botonPloteoGerenal.setToolTip(str_help["F1: boton grafico general"].decode('utf-8'))
        botonPloteoGerenal.setEnabled(False)
        self.cBox_Acciones.append(botonPloteoGerenal)

        self.cBox_eventos = []

#######################  FASE 2: DETECCIÓN DE EVENTOS  #######################

        fila_actual += 2
        Texto_det_event = QtGui.QLabel(u'Fase 2: Detección de eventos',self)
        Texto_det_event.move(columna_1, fila_inicial + 20*fila_actual)
        Texto_det_event.resize(Texto_det_event.sizeHint())
        Texto_det_event.setToolTip(str_help["F2: titulo"].decode('utf-8'))

        Texto_det_event.setEnabled(False)
        self.cBox_eventos.append(Texto_det_event)

###############################################################################

        fila_actual += 1
        self.Texto_fase_2 = QtGui.QLabel('Criterio de los eventos de incendio:',self)
        self.Texto_fase_2.move(columna_1 , fila_inicial + 20*fila_actual)
        self.Texto_fase_2.resize(self.Texto_fase_2.sizeHint())
        self.Texto_fase_2.setToolTip(str_help["F2: criterios"].decode('utf-8'))
        self.Texto_fase_2.setEnabled(False)
        self.cBox_eventos.append(self.Texto_fase_2)

###############################################################################

        fila_actual += 1
        Widget_param = QtGui.QWidget(self)
        Widget_param.resize(300,50)
        Widget_param.move(columna_1 + offset_col, fila_inicial + 20*fila_actual)

        self.etiqueta_dist = QtGui.QLabel(u"Parámetro espacial:" ,Widget_param)
        self.etiqueta_dist.resize(140,20)
        self.etiqueta_dist.move(0,0)
        self.line_edit_dist = QtGui.QLineEdit(Widget_param)
        self.cBox_eventos.append(self.line_edit_dist)
        self.line_edit_dist.resize(80,20)
        self.line_edit_dist.move(150, 0)
        self.line_edit_dist.setToolTip(str_help["F2: espacial"].decode('utf-8'))
        self.etiqueta_metros = QtGui.QLabel("m" ,Widget_param)
        self.etiqueta_metros.resize(60,20)
        self.etiqueta_metros.move(235,0)

        self.etiqueta_dias = QtGui.QLabel(u"Parámetro temporal:" ,Widget_param)
        self.etiqueta_dias.resize(140,20)
        self.etiqueta_dias.move(0,25)
        self.line_edit_dias = QtGui.QLineEdit(Widget_param)
        self.cBox_eventos.append(self.line_edit_dias)
        self.line_edit_dias.resize(80,20)
        self.line_edit_dias.move(150, 25)
        self.line_edit_dias.setToolTip(str_help["F2: temporal"].decode('utf-8'))
        self.etiqueta_dias = QtGui.QLabel(u"días" ,Widget_param)
        self.etiqueta_dias.resize(60,20)
        self.etiqueta_dias.move(235,25)

        Widget_param.setEnabled(False)
        self.cBox_eventos.append(Widget_param)

###############################################################################

        fila_actual += 3
        botonDetectarEventos = QtGui.QPushButton('Procesar', self)
        botonDetectarEventos.resize(botonDetectarEventos.sizeHint())
        botonDetectarEventos.move(columna_1 + offset_col, fila_inicial + 20*fila_actual)
        botonDetectarEventos.clicked.connect(self.Procesamiento_Eventos)
        botonDetectarEventos.setToolTip(str_help["F2: procesar"].decode('utf-8'))
        botonDetectarEventos.setEnabled(False)
        #botonDetectarEventos.hide()
        self.cBox_eventos.append(botonDetectarEventos)

###############################################################################

        fila_actual += 2

        self.cBox_ploteos = []
        Texto_ploteo = QtGui.QLabel(u'Seleccionar el tipo de gráfico a realizar:',self)
        Texto_ploteo.move(columna_1 , fila_inicial + 20*fila_actual)
        Texto_ploteo.resize(Texto_ploteo.sizeHint())

        Texto_ploteo.setEnabled(False)
        self.cBox_ploteos.append(Texto_ploteo)

###############################################################################

        fila_actual += 1
        for j in range(len(tipos_ploteos)):
            checkBoxAux = QtGui.QCheckBox(tipos_ploteos[j], self)
            checkBoxAux.move(columna_1 + offset_col +(j%3)*90, fila_inicial + 20*(int(j/3)+fila_actual))
            checkBoxAux.setToolTip(str_help[plot_dic[j]].decode('utf-8'))
#            checkBoxAux.move(columna_1 + offset_col, fila_inicial + 20*(fila_actual+j))
            checkBoxAux.resize(checkBoxAux.sizeHint())
            checkBoxAux.setEnabled(False)
            self.cBox_ploteos.append(checkBoxAux)

###############################################################################

        fila_actual += len(tipos_ploteos)/2 + 0.5
        botonPloteoEventos = QtGui.QPushButton('Graficar Eventos', self)
        botonPloteoEventos.resize(botonPloteoEventos.sizeHint())
        botonPloteoEventos.move(columna_1 + offset_col, fila_inicial + 20*fila_actual)
        botonPloteoEventos.clicked.connect(self.Ploteo_Selectivo)
        botonPloteoEventos.setToolTip(str_help["F2: boton grafico"].decode('utf-8'))
        botonPloteoEventos.setEnabled(False)
        self.cBox_ploteos.append(botonPloteoEventos)


###############################################################################
###############################################################################
###############################################################################

###################  FASE 3: ANÁLISIS CON IMÁGENES RASTER  ###################


# segunda columna, deteccion de eventos
        fila_actual = 0
        columna_2 = 450

        self.cBox_raster = []

        Texto_det_event = QtGui.QLabel(u'Fase 3: Análisis con imágenes Raster',self)
        Texto_det_event.move(columna_2, fila_inicial + 20*fila_actual)
        Texto_det_event.resize(Texto_det_event.sizeHint())
        Texto_det_event.setToolTip(str_help["F3: titulo"].decode('utf-8'))
        Texto_det_event.setEnabled(False)
        self.cBox_raster.append(Texto_det_event)

###############################################################################

        fila_actual += 1
        Texto_sel_event = QtGui.QLabel(u'Lista de eventos detectatos.\nSeleccionar \
de la siguiente lista el eventos a analizar:',self)
        Texto_sel_event.move(columna_2, fila_inicial + 20*fila_actual)
        Texto_sel_event.resize(Texto_sel_event.sizeHint())
        Texto_sel_event.setEnabled(False)
        self.cBox_raster.append(Texto_sel_event)

###############################################################################

        fila_actual += 2
        Widget_tablaEv = QtGui.QWidget(self)
        Widget_tablaEv.move(columna_2 + offset_col, fila_inicial + 20*fila_actual)
        Widget_tablaEv.resize(350,200)
        self.tabla = QtGui.QTableWidget(Widget_tablaEv)
        self.tabla.resize(350,200)
        self.tabla.setColumnCount(4)
        self.tabla.setColumnWidth(0,75)
        self.tabla.setColumnWidth(1,75)
        self.tabla.setColumnWidth(2,75)
        self.tabla.setColumnWidth(3,85)
        self.tabla.setHorizontalHeaderLabels(\
                    ['ID Evento',u'N° Focos',u'Área (ha)', u'Perímetro (km)'])
        self.cBox_raster.append(self.tabla)
        Widget_tablaEv.setEnabled(False)
        self.cBox_raster.append(Widget_tablaEv)

###############################################################################

        fila_actual += 11
        Texto_analisis_event = QtGui.QLabel(u'Seleccionar el tipo de imagen \
Raster para realizar el análisis:',self)
        Texto_analisis_event.move(columna_2, fila_inicial + 20*fila_actual)
        Texto_analisis_event.resize(Texto_analisis_event.sizeHint())
        Texto_analisis_event.setEnabled(False)
        self.cBox_raster.append(Texto_analisis_event)


###############################################################################


        fila_actual += 1
        for j in range(len(tipos_raster)):
            checkBoxAux = QtGui.QCheckBox(tipos_raster[j], self)
            checkBoxAux.move(columna_2 + offset_col, fila_inicial + 20*(j+fila_actual) )
            checkBoxAux.resize(checkBoxAux.sizeHint())
            checkBoxAux.setToolTip(str_help[raster_dic[j]].decode('utf-8'))
            checkBoxAux.setEnabled(False)
            self.cBox_raster.append(checkBoxAux)

###############################################################################

        fila_actual += 4
        Texto_analisis_event = QtGui.QLabel(u'Cantidad de imágenes Raster a \
incluir en el análisis, en fechas:',self)
        Texto_analisis_event.move(columna_2, fila_inicial + 20*fila_actual)
        Texto_analisis_event.resize(Texto_analisis_event.sizeHint())
        Texto_analisis_event.setToolTip(str_help["F3: fechas"].decode('utf-8'))
        Texto_analisis_event.setEnabled(False)
        self.cBox_raster.append(Texto_analisis_event)

###############################################################################

        fila_actual += 1
        Widget_fechas_raster = QtGui.QWidget(self)
        Widget_fechas_raster.resize(200,50)
        Widget_fechas_raster.move(columna_2 + offset_col, fila_inicial + 20*fila_actual)

        self.etiqueta_fecha_ant = QtGui.QLabel("Fechas anteriores: " ,Widget_fechas_raster)
        self.etiqueta_fecha_ant.resize(150,20)
        self.etiqueta_fecha_ant.move(0,0)
        self.line_edit_fecha_ant = QtGui.QLineEdit(Widget_fechas_raster)
        self.line_edit_fecha_ant.resize(40,20)
        self.line_edit_fecha_ant.move(160, 0)
        self.cBox_raster.append(self.line_edit_fecha_ant)

        self.etiqueta_fecha_pos = QtGui.QLabel(u"Fechas posteriores: " ,Widget_fechas_raster)
        self.etiqueta_fecha_pos.resize(150,20)
        self.etiqueta_fecha_pos.move(0,25)
        self.line_edit_fecha_pos = QtGui.QLineEdit(Widget_fechas_raster)
        self.line_edit_fecha_pos.resize(40,20)
        self.line_edit_fecha_pos.move(160, 25)
        self.cBox_raster.append(self.line_edit_fecha_pos)

        Widget_fechas_raster.setEnabled(False)
        self.cBox_raster.append(Widget_fechas_raster)

###############################################################################

        fila_actual += 3
        botonAnalizarEvento = QtGui.QPushButton('Analizar', self)
        botonAnalizarEvento.resize(botonAnalizarEvento.sizeHint())
        botonAnalizarEvento.move(columna_2 + offset_col, fila_inicial + 20*fila_actual)
        botonAnalizarEvento.clicked.connect(self.Seleccion_Eventos_Tabla)
        botonAnalizarEvento.setToolTip(str_help["F3: analizar"].decode('utf-8'))
        botonAnalizarEvento.setEnabled(False)
        self.cBox_raster.append(botonAnalizarEvento)

        self.cBox_raster_grafica = []

        botonGraficarAnalisis = QtGui.QPushButton(u'Graficar Análisis', self)
        botonGraficarAnalisis.resize(botonGraficarAnalisis.sizeHint())
        botonGraficarAnalisis.move(columna_2 + 6*offset_col, fila_inicial + 20*fila_actual)
        botonGraficarAnalisis.clicked.connect(self.Grafica_Raster)
        botonGraficarAnalisis.setToolTip(str_help["F3: graficar"].decode('utf-8'))
        botonGraficarAnalisis.setEnabled(False)
        self.cBox_raster_grafica.append(botonGraficarAnalisis)

#        fila_actual += 2
#
#        botonGraficarAnalisis = QtGui.QPushButton(u'Aplicacion ivan 1', self)
#        botonGraficarAnalisis.resize(botonGraficarAnalisis.sizeHint())
#        botonGraficarAnalisis.move(columna_2 + offset_col, fila_inicial + 20*fila_actual)
#        botonGraficarAnalisis.clicked.connect(self.Grafica_Raster_1)
#        botonGraficarAnalisis.setEnabled(False)
#        self.cBox_raster_grafica.append(botonGraficarAnalisis)
#
#        botonGraficarAnalisis = QtGui.QPushButton(u'Aplicacion ivan 2', self)
#        botonGraficarAnalisis.resize(botonGraficarAnalisis.sizeHint())
#        botonGraficarAnalisis.move(columna_2 + 6*offset_col, fila_inicial + 20*fila_actual)
#        botonGraficarAnalisis.clicked.connect(self.Grafica_Raster_2)
#        botonGraficarAnalisis.setEnabled(False)
#        self.cBox_raster_grafica.append(botonGraficarAnalisis)

###############################################################################
###############################################################################
###############################################################################

#################  FASE 4: ANÁLISIS DE CERCANÍA A LOS FOCOS  #################

        fila_actual=0
        columna_3 = 900
        self.cBox_contorno = []

        cBox_contorno = QtGui.QLabel(u'Fase 4: Análisis de cercanía a los focos:',self)
        cBox_contorno.move(columna_3 , fila_inicial + 20*fila_actual)
        cBox_contorno.resize(cBox_contorno.sizeHint())
        cBox_contorno.setToolTip(str_help["F4: titulo"].decode('utf-8'))
        cBox_contorno.setEnabled(False)
        self.cBox_contorno.append(cBox_contorno)

###############################################################################

        fila_actual += 1
        for j in range(len(tipos_contorno)):
            checkBoxAux = QtGui.QCheckBox(tipos_contorno[j], self)
            checkBoxAux.move(columna_3 + offset_col, fila_inicial + 20*(j+fila_actual) )
            checkBoxAux.resize(checkBoxAux.sizeHint())
            checkBoxAux.setEnabled(False)
            self.cBox_contorno.append(checkBoxAux)

###############################################################################


        fila_actual += len(tipos_contorno)+1

        botonContornoEvento = QtGui.QPushButton(u'Analizar cercanías', self)
        botonContornoEvento.resize(botonContornoEvento.sizeHint())
        botonContornoEvento.move(columna_3 + offset_col, fila_inicial + 20*fila_actual)
        botonContornoEvento.clicked.connect(self.Analisis_Contorno)
        botonContornoEvento.setToolTip(str_help["F4: analizar"].decode('utf-8'))
        botonContornoEvento.setEnabled(False)
        self.cBox_contorno.append(botonContornoEvento)

###############################################################################

###################  FASE 5: SELECCIÓN DE ITEMS A GUARDAR  ###################

        self.cBox_guardar = []
        fila_actual += 2
        Texto_guardar = QtGui.QLabel(u'Fase 5: Selección de items a guardar:',self)
        Texto_guardar.move(columna_3 , fila_inicial + 20*fila_actual)
        Texto_guardar.resize(Texto_guardar.sizeHint())
        Texto_guardar.setToolTip(str_help["F5: titulo"].decode('utf-8'))
        Texto_guardar.setEnabled(False)
        self.cBox_guardar.append(Texto_guardar)

###############################################################################


        fila_actual += 1
        for j in range(len(tipos_guardar)):
            checkBoxAux = QtGui.QCheckBox(tipos_guardar[j], self)
            checkBoxAux.move(columna_3 + offset_col, fila_inicial + 20*(j+fila_actual) )
            checkBoxAux.resize(checkBoxAux.sizeHint())
            checkBoxAux.setToolTip(str_help[guardar_dic[j]].decode('utf-8'))
            checkBoxAux.setEnabled(False)
            self.cBox_guardar.append(checkBoxAux)


        self.cBox_guardar_especial = []
        fila_actual += len(tipos_guardar)
        for j in range(len(tipos_guardar_especial)):
            checkBoxAux = QtGui.QCheckBox(tipos_guardar_especial[j], self)
            checkBoxAux.move(columna_3 + offset_col, fila_inicial + 20*(j+fila_actual) )
            checkBoxAux.resize(checkBoxAux.sizeHint())
            checkBoxAux.setToolTip(str_help[guardar2_dic[j]].decode('utf-8'))
            checkBoxAux.setEnabled(False)
            self.cBox_guardar_especial.append(checkBoxAux)

        fila_actual += len(tipos_guardar_especial)+1
        Widget_nombre = QtGui.QWidget(self)
        Widget_nombre.resize(260, 50)
        Widget_nombre.move(columna_3 + offset_col, fila_inicial + 20*fila_actual)

        self.etiqueta_nom = QtGui.QLabel("Nombre del proyecto:" ,Widget_nombre)
        self.etiqueta_nom.resize(160, 20)
        self.etiqueta_nom.move(0, 0)
        self.line_edit_nom = QtGui.QLineEdit(Widget_nombre)
        self.line_edit_nom.resize(200, 20)
        self.line_edit_nom.move(0, 25)
        self.line_edit_nom.setToolTip(str_help["F5: nombre"].decode('utf-8'))
        self.cBox_guardar.append(self.line_edit_nom)


        Widget_nombre.setEnabled(False)
        self.cBox_guardar.append(Widget_nombre)

###############################################################################

        fila_actual += 3
        botonGuardarEvento = QtGui.QPushButton('Guardar', self)
        botonGuardarEvento.resize(botonGuardarEvento.sizeHint())
        botonGuardarEvento.move(columna_3 + offset_col, fila_inicial + 20*fila_actual)
        botonGuardarEvento.clicked.connect(self.Guardado_Selectivo)
        botonGuardarEvento.setToolTip(str_help["F5: Guardar"].decode('utf-8'))
        botonGuardarEvento.setEnabled(False)
        self.cBox_guardar.append(botonGuardarEvento)


###############################################################################

#        fila_actual += 3
#
#        Widget_progreso = QtGui.QWidget(self)
#        Widget_progreso.resize(260, 50)
#        Widget_progreso.move(columna_3 + offset_col, fila_inicial + 20*fila_actual)
#
#
#        #Se crea la instancia de la barra de progreso.
#        self.pbar = QtGui.QProgressBar(Widget_progreso)
#        self.pbar.move(0, 0)
#
#        #Se crea la instancia timer con el contador en cero
#        self.timer = QtCore.QBasicTimer()
#        self.step = 0



###############################################################################

        # Genero una barra de herramientas de acceso rapido.
        self.toolbar1 = self.addToolBar('Herramientas')
        self.crear_accesos_toolbar(dir_iconos+'icono_sepa.png',str_help["F0: abrir sepa"].decode('utf-8'),'Ctrl+A', \
                                    "SEPA", self.toolbar1,self.Open_sepa)
        self.crear_accesos_toolbar(dir_iconos+'icono_modis.png',str_help["F0: abrir modis"].decode('utf-8'),'Ctrl+S', \
                                    "MODIS", self.toolbar1,self.Open_modis)
        self.crear_accesos_toolbar(dir_iconos+'icono_modis_shp.png',str_help["F0: abrir modis shp"].decode('utf-8'),'Ctrl+D', \
                                    'MODIS SHP', self.toolbar1,self.Open_modis_shp)
        self.crear_accesos_toolbar(dir_iconos+'icono_config.png',str_help["F0: abrir config"].decode('utf-8'),'Ctrl+E', \
                                    u'Configuración', self.toolbar1,self.Ventana_Configuracion)
        self.crear_accesos_toolbar(dir_iconos+'icono_test.png',str_help["F0: test"].decode('utf-8'),'Ctrl+T', \
                                   u'Verificación rápida', self.toolbar1,self.Testeo_Rapido)
        self.crear_accesos_toolbar(dir_iconos+'icono_licen.png',str_help["F0: abrir licencia"].decode('utf-8'),'Ctrl+E', \
                                   'Licencia', self.toolbar1,self.Ventana_Licencia)
        self.crear_accesos_toolbar(dir_iconos+'icono_help.png',str_help["F0: abrir manual"].decode('utf-8'),'Ctrl+Q', \
                                   'Manual', self.toolbar1,self.Manual)
        self.crear_accesos_toolbar(dir_iconos+'icono_salir.png',str_help["F0: salir"].decode('utf-8'),'Ctrl+Q', \
                                   'Salir', self.toolbar1,QtGui.qApp.quit)

###############################################################################
        # Habilito la barra de estado.
        self.statusBar()

#        menubar = self.menuBar()
#        fileMenu = menubar.addMenu('&Archivo')
#        fileMenu.addAction(openFile)

        self.setGeometry(300, 300, 3500, 3000)
        self.setWindowTitle('DyCEI')
        self.setWindowIcon(QtGui.QIcon(dir_iconos+'fuego.png'))
        self.show()

#    def timerEvent(self, event):
#        #funcion asociada al timer.
#        #Si el contador llega a 100
#        #Se detiene el timer, se cambia el titulo
#        #del boton, se coloca el contador en cero
#        #y se sale de la funcion.
#        #Si no llega a 100, se incrementa en 1 el contador y
#        #se le asigna un nuevo valor a la barra de progreso.
#        [total,parcial] = Decoder_Estado()
#        print parcial,"/",total
#        if total <= parcial:
#            self.timer.stop()
#            #self.button.setText('Iniciar')
#            #self.step = 0
#            return
#        #self.step = parcial/total
#        self.pbar.setValue(int(parcial/total))
#
#    def Accion_Timer(self):
#        #Si el timer esta activo se detiene y se
#        #le cambia el titulo al boton con Iniciar.
#        print "accion timer -------------------------"
#        if self.timer.isActive():
#            self.timer.stop()
#            #self.button.setText('Iniciar')
#        else:
#            #Si no esta activo el timer, se inicia con valor de 100
#            #se coloca el titulo detener al boton.
#            self.timer.start(100, self)
#            #self.button.setText('Detener')

    def Testeo_Rapido(self):
        self._testeo = True

        self.Open_and_Decode()
        self.Filtrar_datos()
        self.Procesamiento_Eventos()
        #self.Seleccion_Eventos_Tabla()
        #self.Ploteo_Selectivo()
        #self.Guardado_Selectivo()

        self._testeo = False

    def Manual(self):
        archivo = os.popen(dir_manual)

    def Ventana_Configuracion(self):
        self.ventana.exec_()

    def Ventana_Licencia(self):
        self.licencia.exec_()

###############################################################################
##  Se definen los tres tipos de open de los archivos mod14

    def Open_sepa(self):
        self.tipo_mod14 = "sepa"
        self.Open_and_Decode()

    def Open_modis(self):
        self.tipo_mod14 = "modis"
        self.Open_and_Decode()

    def Open_modis_shp(self):
        self.tipo_mod14 = "modis shp"
        self.Open_and_Decode()

###############################################################################
    def Open_and_Decode(self):
        """Metodo que se encarga de abrir y decodificar la información que
        le viene dada en el archivo que abre
        """


    ##  Almaceno el directorio y en nombre del archivo que quiero abrir
        if (self._testeo == False):

            if(self.tipo_mod14 == "sepa"):
                fname = QtGui.QFileDialog.getOpenFileName(self,txt_open_sepa,
                    pathActual)

            elif(self.tipo_mod14 == "modis"):
                fname = QtGui.QFileDialog.getOpenFileName(self,txt_open_modis,
                    pathActual)

            elif(self.tipo_mod14 == "modis shp"):
                fname = QtGui.QFileDialog.getOpenFileName(self,txt_open_modis2,
                    pathActual)

        else:
            self.tipo_mod14 = "sepa"
            fname = dir_testeo_rap


        # Verifico si el mismo existe
        try:
            if os.path.exists(fname):

                if(self.tipo_mod14 == "sepa" and  \
                            ( fname[-4:] == ".csv" or fname[-4:] == ".txt") ):
                    [self.lista_in,self.estado ] = \
                                    Decoder(str(fname),self.tipo_mod14)

                elif(self.tipo_mod14 == "modis" and  \
                            ( fname[-4:] == ".csv" or fname[-4:] == ".txt") ):
                    [self.lista_in,self.estado ] = \
                                    Decoder(str(fname),self.tipo_mod14)

                elif(self.tipo_mod14 == "modis shp" and fname[-4:] == ".shp" ):
                    [self.lista_in,self.estado ] = \
                                    Decoder(str(fname),self.tipo_mod14)
                else:
                    self.estado = 1
            else:
                self.estado = 2
        except:
            self.estado = 2

        if(self.estado != 0):
            self.Notificar(str_error[self.estado])
            return

        # Intercambio los textos de inicio
        self.Ocultar_Box([self.Texto_inicio])
        self.Mostrar_Box([self.Texto_fase_1])
        self.Habilitar_Box(self.cBox_provincias)
        self.estado = 0

###############################################################################
    def Filtrar_datos(self):
        """Metodo que se encarga de adquirir los items de filtrado, provincia
        fecha inicial y fecha final. Luego normaliza los datos para trabajarlos
        de la manera mas adecuada.
        """
        self.provincias_filtradas = []

        if(self._testeo == False):
            for i in range(len(provincias)):
                if(self.cBox_provincias[i+2].isChecked()):
                    self.provincias_filtradas.append(provincias[i])


            self.str_fecha_ini = str(self.fecha_inicial.date().year()).zfill(4) + \
                        str(self.fecha_inicial.date().month()).zfill(2) + \
                        str(self.fecha_inicial.date().day()).zfill(2)

            self.str_fecha_fin = str(self.fecha_final.date().year()).zfill(4) + \
                        str(self.fecha_final.date().month()).zfill(2) + \
                        str(self.fecha_final.date().day()).zfill(2)
        else:
            self.provincias_filtradas.append("San Luis")
            self.str_fecha_ini = "20160101"
            self.str_fecha_fin = "20170101"

            for i in range(len(provincias)):
                if(self.cBox_provincias[i+2].isChecked()):
                    self.cBox_provincias[i+2].click()
            self.cBox_provincias[8].click()
            self.fecha_inicial.setDate(QtCore.QDate(2016,01,01))
            self.fecha_final.setDate(QtCore.QDate(2017,01,01))

        if (self.str_fecha_ini > self.str_fecha_fin):
            self.estado = 7
            self.Deshabilitar_Box(self.cBox_Acciones)
            self.Notificar(str_error[self.estado])
            return

        self.Normalizar_lista()
        self.estado = 0

###############################################################################
    def Normalizar_lista(self):
        """Metodo que normaliza los datos de tal manera que genere un registro
        de todos los datos ya filtrados.
        """
        self.estado = 0
        # Se crea la lista de objetos Foco
        self.lista_normalizada, self.prov_analizadas, self.estado =\
                Norma(self.lista_in, self.provincias_filtradas, \
                      self.str_fecha_ini, self.str_fecha_fin , 4, self.tipo_mod14)
        if(self.estado != 0):
            self.Notificar(str_error[self.estado])
            if(self.estado != 9 ):
                return

        self.Habilitar_Box(self.cBox_Acciones)
        self.Deshabilitar_Box(self.cBox_eventos)
        self.Habilitar_Box(self.cBox_eventos)
        self.Deshabilitar_Box(self.cBox_guardar_especial)
        self.Deshabilitar_Box(self.cBox_guardar)
        self.Deshabilitar_Box(self.cBox_ploteos)
        self.Deshabilitar_Box(self.cBox_raster)
        self.Deshabilitar_Box(self.cBox_contorno)
        self.cBox_analisis_raster = []

###############################################################################
    def Procesamiento_Eventos(self):
        """Metodo que se encarga de adquirir las ventanas espacial y temporal,
        criterios a la hora de identificar los eventos de incendios.
        """
        self.estado = 0

        if(self._testeo == False):
            try:
                param_dist = int(self.line_edit_dist.text())
                param_dias = int(self.line_edit_dias.text())
            except:
                self.estado = 10

        else:
            param_dist = 2000
            param_dias = 3
            self.line_edit_dist.insert(str(param_dist))
            self.line_edit_dias.insert(str(param_dias))

        if (self.estado == 0):
            # Se detectan los eventos de incendio.
            # Cada evento es una listas de objetos Foco
            [self.lista_eventos, self.cant_eventos,self.lista_velocidades] \
                = DetEventos(self.lista_normalizada, param_dist, param_dias)

            # Se crea la lista de objetos Evento

            self.lista_Ev = [Evento(self.lista_eventos[i], \
                chr(int(i/(26*26))%26+65)+chr(int(i/26)%26+65)+chr((i%26)+65), \
                self.lista_velocidades[i]) \
                for i in range(self.cant_eventos)]

            self.Actualizar_Tabla_Eventos()


            self.Habilitar_Box(self.cBox_ploteos)
            self.Habilitar_Box(self.cBox_guardar)
            self.Habilitar_Box(self.cBox_contorno)
            self.Deshabilitar_Box(self.cBox_raster)
            self.Habilitar_Box(self.cBox_raster)
            self.Deshabilitar_Box(self.cBox_guardar_especial)
            self.cBox_analisis_raster = []

        else:
            self.Notificar(str_error[self.estado])
            return

###############################################################################
    def Ploteo_General(self):
        # Se muestran los focos de calor
        PlotFocos(self.lista_normalizada,\
                    "Funcion PlotFocos() con la lista de todos los focos")

###############################################################################
    def Ploteo_Selectivo(self):
        seleccion = []

        if(self._testeo == False):
            for i in range(len(tipos_ploteos)):
                if(self.cBox_ploteos[i+1].isChecked()):
                    seleccion.append(tipos_ploteos[i])
        else:
            seleccion.append(tipos_ploteos[1])
            self.cBox_ploteos[2].click()


        if (tipos_ploteos[0] in seleccion):
            # Se muestran los eventos de incendio
            PlotEventos(self.lista_Ev, \
            u"Representación de los eventos", True)

        if (tipos_ploteos[1] in seleccion):
            # Se muestran los eventos de incendio
            PlotPropagacion(self.lista_Ev, \
            u"Representación de la propagación de los eventos", True)

        if (tipos_ploteos[2] in seleccion):
            PlotMesh(self.lista_Ev)

###############################################################################
    def Analisis_Contorno(self):
        seleccion = []
        estados = []
        self.estado = 0
        for i in range(len(tipos_contorno)):
            if(self.cBox_contorno[i+1].isChecked()):
                seleccion.append(tipos_contorno[i])

        if(len(seleccion) == 0):
            self.estado = 11
            estados.append(self.estado)

        if (self.estado == 0):
            if (tipos_contorno[0] in seleccion):
                [self.lista_Ev,self.estado] =  \
                            Comparar_Entradas_Shp(self.lista_Ev, self.prov_analizadas)
                estados.append(self.estado)

            if (tipos_contorno[1] in seleccion):
                [self.lista_Ev,estado] =  \
                            Adjuntar_Rutas_Deptos(self.lista_Ev)
                estados += estado ## devuelve una lista
            if (tipos_contorno[2] in seleccion):
                Adjuntar_Localidad(self.lista_Ev)


        msj_error = ""
        for i_estados in range(len(estados)):
            if(estados[i_estados] != 0):
                msj_error += str_error[estados[i_estados]]+'\n'

        if (msj_error != ""):
            self.Notificar(msj_error)
            return

###############################################################################
    def Guardado_Selectivo(self):
        seleccion = []
        self.estado = 0

        if(self._testeo == False):
            for i in range(len(tipos_guardar)):
                if(self.cBox_guardar[i+1].isChecked()):
                    seleccion.append(tipos_guardar[i])

            for i in range(len(tipos_guardar_especial)):
                if(self.cBox_guardar_especial[i].isChecked()):
                    seleccion.append(tipos_guardar_especial[i])

            if(len(seleccion) == 0):
                self.estado = 11
                self.Notificar(str_error[self.estado])
                return

            fname = QtGui.QFileDialog.getExistingDirectory(self, 'Selecionar directorio', \
                    pathActual)
        else:
            seleccion.append(tipos_guardar[0])
            seleccion.append(tipos_guardar[1])
            self.cBox_guardar[1].click()
            self.cBox_guardar[2].click()
            fname = dir_salidas


        if os.path.exists(fname):
            pass
        else:
            self.estado = 2
            self.Notificar(str_error[self.estado])
            return

        if len(self.line_edit_nom.text()) == 0:
            self.estado = 13
            nombre = "Proyecto_shp"
            self.line_edit_nom.setText(nombre)
        else:
            nombre = self.line_edit_nom.text()

        if(self.estado != 0):
            self.Notificar(str_error[self.estado])

        estados = ToFile(self.lista_Ev,nuevo_directorio= fname, \
                nombre_proyecto= nombre, guardar_seleccion= seleccion, \
                objetos_raster = self.cBox_analisis_raster)

        msj_error = ""
        for i_estados in range(len(estados)):
            if(estados[i_estados] != 0):
                if(estados[i_estados] != 17):
                    msj_error += str_error[estados[i_estados]]+'\n'
                else:
                    self.Notificar(str_error[17])
                    continue

        if (msj_error != ""):
            self.Notificar(msj_error)


###############################################################################
    def Actualizar_Tabla_Eventos(self):

        # La siguiente linea se utiliza para resetear la tabla
        self.tabla.setRowCount(0)


        for index in range(len(self.lista_Ev)):
            self.tabla.insertRow(index)
            id_evento  = QtGui.QTableWidgetItem(str(self.lista_Ev[index].id))
            cant_focos = QtGui.QTableWidgetItem(str(self.lista_Ev[index].CantFocos()))
            area       = QtGui.QTableWidgetItem(str(self.lista_Ev[index].Area()))
            perimetro  = QtGui.QTableWidgetItem(str(self.lista_Ev[index].Perimetro()))

            self.tabla.setItem(index,0,id_evento)
            self.tabla.setItem(index,1,cant_focos)
            self.tabla.setItem(index,2,area)
            self.tabla.setItem(index,3,perimetro)

###############################################################################
    def Seleccion_Eventos_Tabla(self):
        """ Funcion encargada de adquirir los datos seteados por el usuario
        para iniciar el análisis raster. Los parametros seleccionados son:
            - Evento/s
            - Tipo de Raster
            - Directorio de los Rasters
            - Fechas anteriores y posteriores a analizar
        """

#------------------------------------------------------------------------------
## Inicio de obtencion y verificacion de los parametros establecidos por el
## usuario

        seleccion = []
        self.estado = 0
        estados = []
        if(self._testeo == False):
            rows = self.tabla.selectionModel().selectedRows()
            indices_ev_sel = []
            for i in rows:
                indices_ev_sel.append(i.row())

            if len(indices_ev_sel) == 0:
                self.estado = 18
                self.Deshabilitar_Box(self.cBox_guardar_especial)
            estados.append(self.estado)

            for i in range(len(tipos_raster)):
                if(self.cBox_raster[i+5].isChecked()):
                    seleccion.append(tipos_raster[i])

            self.estado = 0
            if len(seleccion) == 0:
                self.estado = 19
                self.Deshabilitar_Box(self.cBox_guardar_especial)
            estados.append(self.estado)

            self.estado = 0
            if(tipos_raster[2] in seleccion) and (len(seleccion) == 1): #GLC2000
                param_fechas_ant = 1
                param_fechas_pos = 1
                pass
            else:
                try:
                    param_fechas_ant = int(self.line_edit_fecha_ant.text())
                    param_fechas_pos = int(self.line_edit_fecha_pos.text())
                    if(param_fechas_ant == 0)or(param_fechas_pos == 0):
                        self.estado = 21
                except:
                    self.estado = 20
                    self.Deshabilitar_Box(self.cBox_guardar_especial)
                estados.append(self.estado)

            msj_error = ""
            for i_estados in range(len(estados)):
                if(estados[i_estados] != 0):
                    msj_error += str_error[estados[i_estados]]+'\n'

            if (msj_error != ""):
                self.Notificar(msj_error)
                return
#------------------------------------------------------------------------------
## Inicio de creacion de los objetos Matriz segun el tipo de raster
## seleccionado por el usuario.

            self.cBox_analisis_raster = []
            estados = []

            if(tipos_raster[0] in seleccion): #MOD11
                for indice_ev in indices_ev_sel:
                    auxMatriz = MatrizMOD11(self.lista_Ev[indice_ev],param_fechas_ant, \
                                param_fechas_pos,self.ventana.dir_mod11)
                    self.estado = auxMatriz.Estado()
                    if(self.estado == 0):

                        self.cBox_analisis_raster.append(auxMatriz)
                        self.Habilitar_Box(self.cBox_guardar_especial)
                        self.Habilitar_Box(self.cBox_raster_grafica)

                    elif(self.estado != 30):
                        estados.append(self.estado)
                        if(self.estado != 33) and (self.estado != 36):

                            self.cBox_analisis_raster.append(auxMatriz)
                            self.Habilitar_Box(self.cBox_guardar_especial)
                            self.Habilitar_Box(self.cBox_raster_grafica)
                    else:
                        estados.append(self.estado + 2)


            if(tipos_raster[1] in seleccion): #MOD13
                for indice_ev in indices_ev_sel:
                    auxMatriz = MatrizMOD13(self.lista_Ev[indice_ev],param_fechas_ant, \
                                param_fechas_pos,self.ventana.dir_mod13)
                    self.estado = auxMatriz.Estado()
                    if(self.estado == 0):

                        self.cBox_analisis_raster.append(auxMatriz)
                        self.Habilitar_Box(self.cBox_guardar_especial)
                        self.Habilitar_Box(self.cBox_raster_grafica)

                    elif(self.estado != 30):
                        estados.append(self.estado + 6)
                        if(self.estado != 33) and (self.estado != 36):

                            self.cBox_analisis_raster.append(auxMatriz)
                            self.Habilitar_Box(self.cBox_guardar_especial)
                            self.Habilitar_Box(self.cBox_raster_grafica)
                    else:
                        estados.append(self.estado + 1)


            if(tipos_raster[2] in seleccion): #GLC2000
                for indice_ev in indices_ev_sel:
                    auxMatriz = MatrizGLC2000(self.lista_Ev[indice_ev],param_fechas_ant, \
                                param_fechas_pos,self.ventana.dir_glc2000)
                    self.estado = auxMatriz.Estado()
                    if(self.estado == 0):

                        self.cBox_analisis_raster.append(auxMatriz)
                        self.Habilitar_Box(self.cBox_guardar_especial)
                        self.Habilitar_Box(self.cBox_raster_grafica)
                    else:
                        estados.append(self.estado)


            msj_error = ""
            for i_estados in range(len(estados)):
                if(estados[i_estados] != 0):
                    msj_error += str_error[estados[i_estados]]+'\n'

            if (msj_error != ""):
                self.Notificar(msj_error)
                return
#------------------------------------------------------------------------------
##  Ejecucion del código en modo testeo rápido
        else:
            print u"Verificación rápida analiza los Rásters"
            self.cBox_guardar_especial[0].click()
            seleccion.append(tipos_raster[0])
            self.cBox_raster[5].click()
            self.tabla.selectRow(0)
            self.ubicacion_raster =  dir_rasters
            if os.path.exists(self.ubicacion_raster):
                self.line_edit_ubic.setText(str(self.ubicacion_raster))

            param_fechas_ant = 1
            param_fechas_pos = 2
            self.line_edit_fecha_ant.insert(str(param_fechas_ant))
            self.line_edit_fecha_pos.insert(str(param_fechas_pos))
            self.cBox_analisis_raster = [MatrizNDVI(self.lista_Ev[0], \
                    param_fechas_ant,param_fechas_pos,self.ventana.dir_mod13)]

            self.Habilitar_Box(self.cBox_guardar_especial)
            self.Habilitar_Box(self.cBox_raster_grafica)

###############################################################################
    def Grafica_Raster(self):
        for indice in range(len(self.cBox_analisis_raster)):
            self.cBox_analisis_raster[indice].GraficarDatos()

    def Grafica_Raster_1(self):
        for indice in range(len(self.cBox_analisis_raster)):
            self.cBox_analisis_raster[indice].GraficarDatos(1)

    def Grafica_Raster_2(self):
        for indice in range(len(self.cBox_analisis_raster)):
            self.cBox_analisis_raster[indice].GraficarDatos(2)

###############################################################################
    def Ubicacion_Raster(self):
        self.ubicacion_raster = str(QtGui.QFileDialog.getExistingDirectory(self, 'Selecionar directorio', \
            pathActual))


        if os.path.exists(self.ubicacion_raster):
            self.line_edit_ubic.setText(str(self.ubicacion_raster))
        else:
            print "Problema el fichero no existe"
            return

###############################################################################
    def Habilitar_Box(self,lista_box):
        for i in range(len(lista_box)):
            lista_box[i].setEnabled(True)

###############################################################################
    def Deshabilitar_Box(self,lista_box):
        for i in range(len(lista_box)):
            if type(lista_box[i]) is QtGui.QCheckBox:
                if lista_box[i].isChecked():
                    lista_box[i].click()

            if type(lista_box[i]) is QtGui.QLineEdit:

                lista_box[i].clear()

            lista_box[i].setEnabled(False)

###############################################################################
    def Mostrar_Box(self,lista_box):
        for i in range(len(lista_box)):
            lista_box[i].show()

###############################################################################
    def Ocultar_Box(self,lista_box):
        for i in range(len(lista_box)):
            lista_box[i].hide()

###############################################################################
    def crear_accesos_toolbar(self,icono,nombre,accDir,comentario,toolbar,accion):
    ##  Creo un objeto accion la cual se le asignara una accion especifica,
    ##  con un acceso directo y un comentario.

        # Se crea la accion exit utilisando el nombre de "Salir"
        exitAction = QtGui.QAction(QtGui.QIcon(icono), nombre, self)

        # Seteo el acceso directo a dicha accion
        exitAction.setShortcut(accDir)

        # Seteo el comentario de dicha accion
        exitAction.setStatusTip(comentario)

        # Agrego a la barra de herramientas
        toolbar.addAction(exitAction)

        # Conecta dicha accion con el metodo quit() para salir de la aplicacion
        exitAction.triggered.connect(accion)


# simulación de una tarea de largo plazo
def actualizar_archivos():
    for __ in xrange(150000000):
        continue

###############################################################################
def main():

    app = QtGui.QApplication(sys.argv)

 ## Pantalla de bienvenida
    # pixmap
#    pixmap = QtGui.QPixmap(os.path.join('imagenes', 'Bienvenidos.jpg'), 'JPG')
    pixmap = QtGui.QPixmap('Bienvenidos.jpg', 'JPG')

    # pantalla de bienvenida
#    pantalla_de_bienvenida = QtGui.QSplashScreen(pixmap, QtCore.WindowStaysOnTopHint)
    pantalla_de_bienvenida = QtGui.QSplashScreen(pixmap, QtCore.Qt.WindowStaysOnTopHint)
    pantalla_de_bienvenida.show()

    # actualizando archivos
    pantalla_de_bienvenida.showMessage('Iniciando programa...', \
                                       QtCore.Qt.AlignBottom, QtCore.Qt.white)
    actualizar_archivos()
    app.processEvents()  # permite a la aplicación recibir eventos de entrada del usuario entre tareas de largo plazo


    ex = Interface_Grafica()
#
    # cierra la pantalla de bienvenida después que la ventana se muestra
    pantalla_de_bienvenida.finish(ex)

    sys.exit(app.exec_())


#if __name__ == '__main__':
main()
###############################################################################

