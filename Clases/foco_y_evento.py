#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Nombre de script:    foco_y_evento.py
Objetivo:            Define las clases foco y evento usadas en el programa
Autor:               Iván
"""
import utm
from matplotlib        import pyplot as plt, tri
from datetime          import date
from numpy             import array, where
from scipy.spatial     import ConvexHull
from math              import acos as arccos, degrees as grados
from shapely.geometry  import Point

"""
###############################################################################
Definición del clase Foco
###############################################################################
"""

class Foco:
    """Clase Foco: Contiene información sobre un foco de calor
    """

    def __init__(self, latitud, longitud, fecha, hora, pais, prov, depto, FRP):
        """Constructor.

        latitud:   latitud donde se detectó el foco

        longitud:  longitud donde se detectó el foco

        fecha:     año, mes y día en el que se detectó el foco (AAAAMMDD o
        AAAA-MM-DD)

        hora:      hora y minuto en el que se detectó el foco (hhmm)

        pais:      país donde se ubicaba el foco

        prov:      provincia donde se ubicaba el foco

        depto:     departamento donde se ubicaba el foco
        
        FRP:       Fire Radiative Power del foco
        """
        self.__lat = float(latitud)
        self.__lon = float(longitud)
        self.__punto_shp = Point( self.__lon, self.__lat)
        self.__reservas = "No hay información"
        self.__rutas = "No hay información"
        self.__localidad = "No hay información"

        fecha = str(fecha)
        self.__anio = int(fecha[0:4])
        if fecha[4] == '-':
            # Tipo MODIS
            self.__mes = int(fecha[5:7])
            self.__dia = int(fecha[8:10])
        else:
            #Tipo Sepa
            self.__mes = int(fecha[4:6])
            self.__dia = int(fecha[6:8])

        hora = str(hora)
        self.__hora = int(hora[0:2])
        self.__min = int(hora[2:4])

        # TODO en caso de errores con los tildes usar decode(encoding='UTF-8',errors='strict')
        self.__pais  = pais
        self.__prov  = prov
        self.__depto = depto
        self.__frp = FRP

    def Cargar_Reserva(self, reserva_afectada):
        if(self.__reservas == "No hay información"):
            self.__reservas = reserva_afectada
        else:
            self.__reservas += " - " + reserva_afectada

    def Cargar_Rutas(self, ruta_afectada):
        if(self.__rutas == "No hay información"):
            self.__rutas = ruta_afectada
        else:
            self.__rutas += " - " + ruta_afectada
            
    def Cargar_Localidad(self, localidad_cerca):
        if(self.__localidad == "No hay información"):
            self.__localidad = localidad_cerca
        else:
            self.__localidad += " - " + localidad_cerca
            
    def Cargar_Depto(self, depto):
        if(self.__depto == "No hay dato"):
            self.__depto = depto

    def Punto_Shp(self):
        """Solo para el uso de Shapefile
        """
        return self.__punto_shp

    def Plot(self, color_relleno = "r", color_borde = "k"):
        """Grafica el foco en el figure actual.

        color_relleno:    el color interno del foco. Predeterminado = rojo

        color_borde:      el color de contorno del foco. Predeterminado = negro
        """
        plt.plot(self.__lon, self.__lat, "o", ms=7, mew=1, mfc=color_relleno,\
            mec=color_borde)

    def Lat(self):
        """Devuelve la latitud en que se detectó el foco (medida en
        grados) en formato número
        """
        return self.__lat

    def Lon(self):
        """Devuelve la longitud en que se detectó el foco (medida en
        grados) en formato número
        """
        return self.__lon

    def Fecha(self,modo = 0):
        """Devuelve la fecha en que se detectó el foco en formato número
        y como ( DD, MM, AAAA )
        """
        if(modo == 0):
            return self.__dia, self.__mes, self.__anio
        else:
            return date(self.__anio, self.__mes, self.__dia)

    def Anio(self):
        """Devuelve el año en que se detectó el foco en formato número
        """
        return self.__anio

    def Mes(self):
        """Devuelve el mes en que se detectó el foco en formato número
        """
        return self.__mes

    def Dia(self):
        """Devuelve el día en que se detectó el foco en formato número
        """
        return self.__dia

    def Hora(self):
        """Devuelve la hora en que se detectó el foco en formato número
        """
        return self.__hora

    def Min(self):
        """Devuelve el minuto en que se detectó el foco en formato número
        """
        return self.__min

    def Pais(self):
        """Devuelve el país en que se detectó el foco en formato texto
        """
        return self.__pais

    def Prov(self):
        """Devuelve la provincia en que se detectó el foco en formato texto
        """
        return self.__prov

    def Depto(self):
        """Devuelve el departamento en que se detectó el foco en formato texto
        """
        return self.__depto

    def FRP(self):
        return self.__frp

    def __str__(self, frase=""):
        """Devuelve los datos en formato texto
        """

        cadena= "|{0:^15}|{2:^15}|{3:^15}|{1:^15}|{4:^15}|{5:^15}|{6:^20}\
|{7:^40}|{8:^20}|{9:^50}|{10:^50}\n".format(self.id,\
            '{0:02}/{1:02}/{2}'.format(self.__dia, self.__mes, self.__anio), \
             self.__lat, self.__lon, self.__pais, self.__prov,self.__depto,\
             self.__localidad,frase, self.__rutas , self.__reservas)
        return cadena

    def UTM(self,forceZoneNumber = None):
        """Devuelve la ubicación del foco en el sistema de coordenadas UTM
        (medida en metros) como X, Y, Huso, Banda
        """
        #  return easting, northing, zone_number, zone_letter

        return utm.from_latlon(self.__lat,self.__lon,forceZoneNumber)

"""
###############################################################################
Definición del clase Evento
###############################################################################
"""
# TODO verificar si conviene que los métodos tengan lazos for que se ejecute
# frecuentemente, o si conviene que el lazo esté en el constructor

class Evento:
    """Clase Evento: Contiene información sobre un evento de incendio
    """

    def __init__(self, lista_focos_del_evento, id, velocidad):
        """Constructor.

        lista_focos_del_evento: es la lista de los focos que conforman el
        evento

        id: identificador del evento, como número o código
        
        velocidad: es la velocidad máxima de propagación adquirida por 
        el evento.
        """
        self.__reservas = "No hay informacion"
        self.__rutas = "No hay informacion"
        self.__localidad = "No hay informacion"
        self.__list_reservas = []
        self.__list_rutas = []
        self.__list_localidad = []

        self.__cantFocos = len(lista_focos_del_evento)
        # Ordeno los focos por orden de ocurrencia, de inicial a final
        self.Foco = sorted(lista_focos_del_evento, key=lambda Foco: \
            # Prioridad de ordenamiento
            (Foco.Anio(), Foco.Mes(), Foco.Dia(), Foco.Hora()))

        # Se obtienen las coordenadas max y min del evento
        # TODO Ver la posibilidad de mmejorar con funciones max o min
        self.__latInf = 9999
        self.__lonIzq = 9999
        self.__latSup = -9999
        self.__lonDer = -9999
        self.ZoneNumbers = []
        for i in range(self.__cantFocos):
            self.ZoneNumbers.append(int((self.Foco[i].Lon() + 180) / 6) + 1)
            if self.Foco[i].Lat() < self.__latInf :
                self.__latInf = self.Foco[i].Lat()
            if self.Foco[i].Lon() < self.__lonIzq :
                self.__lonIzq = self.Foco[i].Lon()
            if self.Foco[i].Lat() > self.__latSup :
                self.__latSup = self.Foco[i].Lat()
            if self.Foco[i].Lon() > self.__lonDer :
                self.__lonDer = self.Foco[i].Lon()

            # Asigno id a cada foco, empezando por el número 1
            # El id del foco se puede modificar
            self.Foco[i].id = i+1  # para letras poner = chr(i+65)

        # Asigno id del evento
        # El id del evento se puede modificar
        self.id = id
        self.__vel_max = velocidad

    def Cargar_Reserva(self, reserva_afectada):
        if(self.__reservas == "No hay informacion"):
            self.__reservas = reserva_afectada
            self.__list_reservas.append(reserva_afectada)
        elif(reserva_afectada not in self.__list_reservas):
            self.__reservas += " - " + reserva_afectada
            self.__list_reservas.append(reserva_afectada)

    def Cargar_Rutas(self, ruta_afectada):
        if(self.__rutas == "No hay informacion"):
            self.__rutas = ruta_afectada
            self.__list_rutas.append(ruta_afectada)
        elif(ruta_afectada not in self.__list_rutas):
            self.__rutas += " - " + ruta_afectada
            self.__list_rutas.append(ruta_afectada)
            
    def Cargar_Localidad(self, localidad_cerca):
        if(self.__localidad == "No hay informacion"):
            self.__localidad = localidad_cerca
            self.__list_localidad.append(localidad_cerca)
        elif(localidad_cerca not in self.__list_localidad):
            self.__localidad += " - " + localidad_cerca
            self.__list_localidad.append(localidad_cerca)

    def CantFocos(self):
        """Devuelve la cantidad de focos del evento en formato número
        """
        return self.__cantFocos

    def FecIni(self):
        """Devuelve la fecha del o de los primer(os) foco(s) del evento
        en formato número y como ( DD, MM, AAAA )
        """
        return self.Foco[0].Fecha()

    def FecFin(self):
        """Devuelve la fecha del o de los último(s) foco(s) del evento
        en formato número y como ( DD, MM, AAAA )
        """
        return self.Foco[ self.__cantFocos - 1 ].Fecha()

    def PrintFecIni(self,modo = 0):
        """Devuelve la fecha del o de los primer(os) foco(s) del evento
        en formato texto y como DD/MM/AAAA
        """
        cadena = "{0:02}/{1:02}/{2}"
        aux = self.FecIni()
        if(modo):

            return cadena.format( aux[0],aux[1],aux[2] )
        else:

            return date(aux[2], aux[1], aux[0])

    def PrintFecFin(self,modo = 0):
        """Devuelve la fecha del o de los último(s) foco(s) del evento
        en formato texto y como DD/MM/AAAA
        """
        cadena = "{0:02}/{1:02}/{2}"
        aux = self.FecFin()
        if(modo):
            return cadena.format( aux[0],aux[1],aux[2] )
        else:
        ##  Devuelve formato de analisis.
            return date(aux[2], aux[1], aux[0])

    def Duracion(self):
        """Devuelve la cantidad de días que duró el evento en formato número.
        Cantidad mínima: 1 día
        """
        t =   date( *list(reversed(self.FecFin())) ) \
            - date( *list(reversed(self.FecIni())) )
        return t.days + 1

    def Coord(self):
        """Devuelve las coordenadas inferiores y superiores en formato número
        y como lista [LatInf, LatSup, LonIzq, LonDer]
        """
        # Si todavía no se ha hecho el cálculo de coordenadas, lo hago

        return [self.__latInf, self.__latSup, self.__lonIzq, self.__lonDer]

    def Velocidad_Maxima(self):
        """Devuelve la velocidad de propagacion máxima del incendio
        """
        return self.__vel_max

    def Plot(self, color_relleno = "g", color_borde = "b", indicar_id = True):
        """Grafica el evento en el figure actual.

        color_relleno: color interno de los focos. Predeterminado = verde

        color_borde: color de contorno de los focos. Predeterminado = azul

        indicar_id: opción de indicar los id de los focos. Predeterminado = True
        """
        for i in range(self.__cantFocos):
            self.Foco[i].Plot(color_relleno, color_borde)

            # Agrego id del foco
            if indicar_id == True:
                plt.text(self.Foco[i].Lon()+0.0015, self.Foco[i].Lat()+0.0015,\
                         self.Foco[i].id, fontsize=8)

    def __str__(self):
        """Devuelve los datos en formato texto
        """

        cadena= "|{0:^15}|{1:^15}|{2:^15}|{3:^15}|{4:^15}|{5:^15}|{6:^15}\
|{7:^15}|{8:^15}|{9:^40}|{10:^49}|{11:^50}\n"
        if(self.__cantFocos > 2):
            aux = cadena.format(self.id, \
                  self.__cantFocos, self.PrintFecIni(1), self.PrintFecFin(1), \
                  self.Area(), self.Perimetro(),self.Duracion(),\
                  self.Velocidad_Maxima(), self.Sentido(), self.__localidad,\
                  self.__rutas,self.__reservas)
        else:
            aux = cadena.format(self.id, \
                  self.__cantFocos,self.PrintFecIni(1), self.PrintFecFin(1), \
                  0, 0,self.Duracion(),self.Velocidad_Maxima(),self.Sentido(),\
                  self.__localidad,self.__rutas, self.__reservas)

        return aux

##############################################################################
#        Métodos para la caracterización de los eventos
##############################################################################

    def IDIni(self):
        """Devuelve el ID del último foco inicial del evento en formato número"""
        # Hago un range(self.__cantFocos) con orden invertido
        for i in range(self.__cantFocos-1, -1, -1):
            if self.Foco[i].Fecha() == self.FecIni() and \
            self.Foco[i].Hora() == self.Foco[0].Hora():
                return i

    def IDFin(self):
        """Devuelve el ID del primer foco final del evento en formato número"""
        # Hago un range(self.__cantFocos) con orden invertido
        for i in range(self.__cantFocos):
            if self.Foco[i].Fecha() == self.FecFin() and \
            self.Foco[i].Hora() == self.Foco[self.__cantFocos-1].Hora():
                return i


    def UbicacionIni(self):
        """Devuelve la ubicación del o de los primer(os) foco(s) del evento
        en formato número

        ( [ lat1,  lat2,  lat3, ... ] ,
          [long1, long2, long3, ... ] )
        """
        return [self.Foco[i].Lat() for i in range(self.__cantFocos) \
                            if self.Foco[i].Fecha() == self.FecFin() ] , \
               [self.Foco[i].Lon() for i in range(self.__cantFocos) \
                            if self.Foco[i].Fecha() == self.FecFin() ]


#        Similar al siguiente código pero para todos los focos iniciales
#        self.Foco[0].Dia(), self.Foco[0].Mes(), self.Foco[0].Anio()
#        return

    def UbicacionFin(self):
        """Devuelve la ubicación del o de los último(s) foco(s) del evento
        en formato número y como array

        ( [ lat1,  lat2,  lat3, ... ] ,
          [long1, long2, long3, ... ] )
        """
        return [self.Foco[i].Lat() for i in range(self.__cantFocos) \
                            if self.Foco[i].Fecha() == self.FecFin() ] , \
               [self.Foco[i].Lon() for i in range(self.__cantFocos) \
                            if self.Foco[i].Fecha() == self.FecFin() ]

    def UbicacionFocos(self):
        """Devuelve la ubicación de todos los focos del evento
        en formato número y como arrays

        ( [ lat1,  lat2,  lat3, ... ] ,
          [long1, long2, long3, ... ] )
        """
        # Método necesario para _Mesh() y para Raster.py
        return [self.Foco[i].Lat() for i in range(self.__cantFocos)], \
               [self.Foco[i].Lon() for i in range(self.__cantFocos)]

    def UbicacionFocos2(self):
        """Devuelve la ubicación de todos los focos del evento
        en formato número y como array, en unidades metro.

        array([[ X1, Y1],
               [ X2, Y2],
               [ X3, Y3],
                   ... ])
        """
        # Método necesario para _CascoConvexo()
        lista = []
        ForceZoneNumber = self._ForceZoneNumber()
        for i in range(self.__cantFocos):
            X, Y, num, key = self.Foco[i].UTM(ForceZoneNumber)
            lista.append([X,Y])
        return array(lista)

    def _ForceZoneNumber(self):
        """Devuelve el numero de zona mas optimo para la proyeccion en metros
        para el uso de las librerias UTM
        """
        # Ordeno
        self.ZoneNumbers.sort()
        # Extraigo el elemento que mas se repite. y fuerzo el numero de zona.
        maxx = 0
        for index in list(set(self.ZoneNumbers)):
            if maxx < self.ZoneNumbers.count(index):
                maxx = self.ZoneNumbers.count(index)
                ForceZoneNumber = index
        return ForceZoneNumber

    def _Mesh(self, radio_min = 0.1):
        """Si el evento tiene al menos 3 focos, devuelve la triangulación
        de Delaunay del evento
        """
        if self.__cantFocos == 2:
            return
        lat,lon =  self.UbicacionFocos()

        # Lo convierto a array
        v_lat = array(lat)
        v_lon = array(lon)

        # Create the Triangulation; no triangles so Delaunay triangulation created.
        try:
            triang = tri.Triangulation(v_lon, v_lat)
        except:
            return None

        # Mask off unwanted triangles.
        lat_m = v_lat[triang.triangles].mean(axis=1)
        lon_m = v_lon[triang.triangles].mean(axis=1)
        mask  = where((lat_m)**2 + (lon_m)**2 < radio_min*radio_min, 1, 0)
        triang.set_mask(mask)
        return triang

    def _CascoConvexo(self):
        try:
            self.__cascoConvexo = ConvexHull(self.UbicacionFocos2())
        except:
            pass

    def Perimetro(self):
        """Devuelve el perímetro del evento (medido en km) en formato número
        """

        """
        La unidad del perímetro obtenida por self.__cascoConvexo.area es:
            m

        Para obtener el resultado en hectáreas se debe multiplicar por:
            - la relación entre km y m:              1 km / 1000 m

        El factor está impuesto por la utilización del Sistema UTM
        medido en metros, y por la unidad deseada.

                  1 km
        N  m  *  ------   =  N * 0.001 km
                 1000 m

        Por ende:
            factor_correccion = 0.001

        Nota: La resolución espacial determina la exactitud, no es factor de
              corrección.
        """
        factor_correccion = 0.001
        try:
            # El método de 'ConvexHull' se llama 'area' pero devuelve el perímetro
            return round(self.__cascoConvexo.area * factor_correccion,1)
        except AttributeError:
            self._CascoConvexo()
            try:
                aux = self.__cascoConvexo.area * factor_correccion
            except:
                aux = 0
        return round(aux,1)

    def Area(self):
        """Devuelve el área del evento (medido en ha) en formato número
        """

        """
        La unidad del área obtenida por self.__cascoConvexo.volume es:
            m2

        Para obtener el resultado en hectáreas se debe multiplicar por:
            - la relación entre ha y m2:             1 ha / 10000 m2

        El factor está impuesto por la utilización del Sistema UTM
        medido en metros, y por la unidad deseada.

                    1 ha
        N  m2  *  --------   =  N * 0.0001 ha
                  10000 m2

        Por ende:
            factor_correccion = 0.0001

        Nota: La resolución espacial determina la exactitud, no es factor de
              corrección.
        """
        factor_correccion = 0.0001
        try:
            # El método de 'ConvexHull' se llama 'volume' pero devuelve el área
            return int(round(self.__cascoConvexo.volume * factor_correccion))
        except AttributeError:
            self._CascoConvexo()
            try:
                aux = self.__cascoConvexo.volume * factor_correccion
            except:
                aux = 0
        return int(round(aux))

    def Sentido(self):
        """Devuelve el sentido medio en grados y en punto cardinal
        """
        """
                                          N
                                   NNO    -    NNE
                                     -    -    -
                               NO     -   -   -      NE
                                  -    -  -  -     -
                           ONO -    -   - - -   -    - ENE
                                   -   - --- -   -
                            O - - - - - - - - - - - - - E
                                   -   - --- -   -
                           OSO -    -   - - -   -    - ESE
                                  -    -  -  -     -
                               SO     -   -   -      SE
                                     -    -    -
                                   SSO    -    SSE
                                          S
        """
        if self.Duracion() == 1:
            return "-"
        suma_lat_ini = 0
        suma_lon_ini = 0
        for j in range(self.__cantFocos):
            if(self.FecIni() == self.Foco[j].Fecha()):
                suma_lat_ini += self.Foco[j].Lat()
                suma_lon_ini += self.Foco[j].Lon()
            else:
                break

        lat_ini = suma_lat_ini/j
        lon_ini = suma_lon_ini/j
        suma_angular = 0
        dist_total = 0
        for i in range(j,(self.__cantFocos)):
            delta_lat = self.Foco[i].Lat() - lat_ini
            delta_lon = self.Foco[i].Lon() - lon_ini
            distancia = (delta_lat**2 + delta_lon**2)**0.5
            try:
                angulo = grados(arccos(delta_lon/distancia))
            except:
                continue
            if (delta_lat < 0):
                angulo = 360 - angulo

            suma_angular += angulo*distancia
            dist_total += distancia

        angulo_final = suma_angular/dist_total
        key = int ( round(angulo_final/22.5) % 16)

        return puntos_cardinales[key]


puntos_cardinales = {0 :"E", 1 :"ENE", 2 :"NE", 3 :"NNE",\
                     4 :"N", 5 :"NNO", 6 :"NO", 7 :"ONO",\
                     8 :"O", 9 :"OSO", 10:"SO", 11:"SSO",\
                     12:"S", 13:"SSE", 14:"SE", 15:"ESE"}