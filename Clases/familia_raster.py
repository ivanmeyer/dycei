#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Nombre de script:    rasters.py
Objetivo:            Define las clases de la familia raster usadas en el programa
Autor:               Daniel e Iván
"""
import os
from datetime              import date, timedelta
import matplotlib.pyplot   as plt
import matplotlib.image    as mpimg
from Modulos.definiciones  import *
from Modulos.raster        import ConsultarDatos, GraficarDatos

slash = sintax_for_os()

"""
###############################################################################
Definición del clase Raster
###############################################################################
"""


class BaseMatriz:
    """Clase base para todo tipo de imagen Raster
    """
    def __init__(self, evento, fechas_anteriores, fechas_posteriores, nombre_unica_img = ""):
        """Constructor.

        evento: objeto evento con los focos a analizar

        fechas_anteriores: cantidad de imágenes adicionales anteriores a la
        fecha inicial del evento

        fechas_posteriores: cantidad de imágenes adicionales posteriores a la
        fecha final del evento
        """
        self.evento   = evento
        self.id  = evento.id
        self.__fechas_ant = fechas_anteriores
        self.__fechas_pos = fechas_posteriores
        self.__cantFocos  = evento.CantFocos()

        self._fechaInicial = evento.PrintFecIni(0)
        self._fechaFinal   = evento.PrintFecFin(0)
        self._lista_rasters_d2 = []
        self._lista_rasters = []
        self._index_fecha_final = -1
        self._index_fecha_inicial = -1
        self._estado = 0

        # Verifico si existe el atributo 'clim', sino lo creo con valor indiferente
        if not hasattr(self, 'clim'):
          self.clim = [None, None]

        if (nombre_unica_img == ""):
            self._estado = self._CodificacionNombreArchivos()

            if (self._estado != 0 ):
                self._estado = self._Verificacion_Errores()
                if (33 != self._estado) and (36 != self._estado):
                    self._Listar_Resultados()

        else:

            self._CodificacionUnSoloArchivo()
            self._archivos_destacados = []
            if(len(self._lista_rasters) != 0):
                for indice in range(len(self._lista_rasters)):
#                    print self._lista_rasters[indice],self._lista_rasters[indice][-4:]

                    if str(self._lista_rasters[indice][-4:]) == ".tif":
                        self._archivos_destacados = [self._lista_rasters[indice]]
                        break

                if len(self._archivos_destacados) == 0:
                    self._estado = 30
            else:
                self._estado = 30

            self.__titulos_fechas = [nombre_unica_img]
            self.__cantFechas = 1


        if (self._estado == 0):
        ##  Creo el formato que voy a implementar.
            self._matrizInterna = {"fecha":[],"dato":[]}
            aux = []


            for i in range(self.__cantFechas):
                self._matrizInterna["fecha"].append(self.__titulos_fechas[i])

                for j in range(self.__cantFocos):
                    aux.append(i*10)
    #            print self._archivos_destacados[i]
                self.__aux = ConsultarDatos(self.folder + self._archivos_destacados[i], \
                                            self.evento, \
                                            self.factor_escala, self.offset)

                self._matrizInterna["dato"].append(self.__aux)


    def _CodificacionNombreArchivos(self):
        """Devuelve una lista de nombres de los archivos existentes según se
        especifique en el método de la clase hijo
        """
        aux = [index for index in os.listdir(self.folder) \
                    if os.path.isfile(os.path.join(self.folder, index))]

        self._lista_rasters = []
        for indice in range(len(aux)):
            if aux[indice][-4:] == ".tif":
                self._lista_rasters.append(aux[indice])

        if (len(self._lista_rasters) == 0 ):
            return 30
    ##  Ordeno la lista con las fechas en forma creciente
        self._lista_rasters.sort()


    ##  Recorto los nombres y me quedo solo con las fechas de las mismas
    ##    self.lista_rasters_d1 = [self.lista_rasters[i].rstrip(".img") \
    ##                           for i in range(len(self.lista_rasters))]

        for i in range(len(self._lista_rasters)):

            ## Verifico si posee formato YYYYMMDD
            try:
                int(self._lista_rasters[i][self._fec_ini:self._fec_ini+8])
                flag_gregoriano = 1
            except:
                ## Verifico si posee formato YYYYDDD
                try:
                    int(self._lista_rasters[i][self._fec_ini:self._fec_ini+7])
                    flag_gregoriano = 0
                except:
                    ## Considero que no existe formato.
                    flag_gregoriano = -1

            if(flag_gregoriano == 1):
                date_aux = date(int(self._lista_rasters[i][self._fec_ini:self._fec_ini+4]), \
                                int(self._lista_rasters[i][self._fec_ini+4:self._fec_ini+6]), \
                                int(self._lista_rasters[i][self._fec_ini+6:self._fec_ini+8]))

                self._lista_rasters_d2.append(date_aux)
                pass

            elif(flag_gregoriano == 0):
                date_aux = date(int(self._lista_rasters[i][self._fec_ini:self._fec_ini+4]), 01 , 01 )
                days = timedelta(days = int(self._lista_rasters[i][self._fec_ini+4:self._fec_ini+7]))
                date_aux = date_aux + days

                self._lista_rasters_d2.append(date_aux)
                pass

            elif(flag_gregoriano == -1):
                self._lista_rasters_d2.append("no data")
                pass


    ##  Ubico el o los Rasters que con fecha inmediatamente menor al evento de
    ##  incendio.

        for i in range(len(self._lista_rasters_d2)):
            #print "fecha raster: " + self.lista_rasters_d1[i] + "fecha inicial: " + self._fechaInicial + "fecha final: " +  self._fechaFinal
            try:
                if( self._lista_rasters_d2[i] <= self._fechaInicial ):
                    self._index_fecha_inicial = i

                if( self._lista_rasters_d2[i] <= self._fechaFinal ):
                    self._index_fecha_final = i
            except:
                continue
        return 0

    def _CodificacionUnSoloArchivo(self):
        """Devuelve una lista de nombres de los archivos existentes según se
        especifique en el método de la clase hijo
        """
        self._lista_rasters = [index for index in os.listdir(self.folder) \
                    if os.path.isfile(os.path.join(self.folder, index))]

    ##  Ordeno la lista con las fechas en forma creciente
        self._lista_rasters.sort()

###############################################################################

    def _Verificacion_Errores(self):
    ##  Verificación de errores (Se debe asegurar la carga de al menos una
    ##  imagen Raster posterior al evento de incendio)
        estado = 0
    #   El evento transcurrió durante fechas anteriores a las fechas de los
    #   Rasters.
        if( self._index_fecha_final == -1 ):
            estado = 33

    #   El evento trancurrió durante fechas posteriores a las fechas de los
    #   Rasters.
        elif( self._index_fecha_inicial == (len(self._lista_rasters)-1) ):
            estado = 36

    #   Faltan Rasters anteriores
        elif( self._index_fecha_inicial == -1 ):
            estado = 34

        elif( (self._index_fecha_inicial +1) < self.__fechas_ant ):
            estado = 35

    #   Faltan Rasters posteriores
        elif( self._index_fecha_final == (len(self._lista_rasters)-1) ):
            estado = 37

        elif( (len(self._lista_rasters)-1) - (self._index_fecha_final ) <= self.__fechas_pos ):
            estado = 38

        return estado
###############################################################################

    def _Listar_Resultados(self):

        self._str_fecha_inicial = str(self._lista_rasters_d2[self._index_fecha_inicial])
    ##  Actualizo el index inicial para que coincida con la primer fecha del
    ##  raster a ser analizado
        if (  (self._index_fecha_inicial - (self.__fechas_ant -1)) < 0 ):
            self._index_fecha_inicial = 0
        else:
            self._index_fecha_inicial -= (self.__fechas_ant -1)

    ##  Actualizo el index final para que coincida con la última fecha del
    ##  raster a ser analizado
        if ( (self._index_fecha_final + self.__fechas_pos) >= len(self._lista_rasters_d2) ):
            self._index_fecha_final = len(self._lista_rasters_d2) -1
        else:
            self._index_fecha_final += self.__fechas_pos

    ##  Almaceno los t{itulus de las columnas de este análisis, en este caso son
    ##  las fechas.
        self.__titulos_fechas = self._lista_rasters_d2[ \
            self._index_fecha_inicial : self._index_fecha_final +1]

    ##  Almaceno los nombres de los archivos rasters a ser analizados.
        self._archivos_destacados = self._lista_rasters[ \
            self._index_fecha_inicial : self._index_fecha_final +1]

    ##  Defino la cantidad de fechas o columnas que va a tener este análisis
        self.__cantFechas = \
            (self._index_fecha_final - self._index_fecha_inicial +1)

###############################################################################

    def MostrarDatos(self, tipo = "no data"):
        """Muestra los datos en la consola
        """

        cadena = "|{0:^18}"
        cadenaTotal = cadena.format("FocoID")

        cadenaTotal = cadenaTotal + cadena.format("FRP")
        for i in range(len(self._matrizInterna["fecha"])):
            cadenaTotal = cadenaTotal + cadena.format( str(self._matrizInterna["fecha"][i]) )
        cadenaTotal = cadenaTotal + "|\n"

        for j in range(self.__cantFocos):
            cadenaTotal = cadenaTotal + cadena.format( str(self.evento.Foco[j].id).zfill(3) )
            cadenaTotal = cadenaTotal + cadena.format(self.evento.Foco[j].FRP())
            for i in range(len(self._matrizInterna["fecha"])):
                cadenaTotal = cadenaTotal + cadena.format(self._matrizInterna["dato"][i][j])
            cadenaTotal = cadenaTotal + "|\n"

        return cadenaTotal


    def GuardarDatos(self, folder_output):
        """Guarda en un archivo txt la información procesada
        """
        file_name = folder_output + self.info +".txt"
        file_raster = open(file_name, 'w+')

        file_raster.write(self.MostrarDatos())
        file_raster.close()
        pass


    def GraficarDatos(self, nro_imagen):
        """Grafica los datos del evento

        nro_imagen: número de la imagen a graficar, desde 1 hasta N.
        N depende de fechas_anteriores, fechas_posteriores y las imágenes
        propias del evento.
        """
        try:
            return GraficarDatos( \
                   self.folder + slash + self._archivos_destacados[nro_imagen-1], \
                   self.evento, self.factor_escala, self.offset, \
                   self.bins, self.absoluto, \
                   self.colormap, self.clim)
        except IndexError:
            print "No se dispone de archivos raster suficientes. Intente con \
otro valor de nro_imagen"

###############################################################################
##
##        Clases de la familia Raster
##
###############################################################################

class MatrizMOD11(BaseMatriz):
    """Clase para manejo de información sobre índice MOD11 obtenida de imágenes Raster
    """
    def __init__(self, evento, fechas_anteriores, fechas_posteriores, folder_input):
        """ Defino el factor de escala y el offset para estas imágenes raster
        """
        self.info = "LST"
        self.factor_escala = 0.02
        self.offset = -273.15    # MODIS indica 0, pero se convierte de K a ºC
        self.bins = 51           # Cantidad de bins del histograma
        self.absoluto = True     # Mapa de colores con rango absoluto o relativo
        self.colormap = 'rainbow'# Mapa de colores
        self.clim = [10, 55]     # Impongo rango de valores mayor a los incluidos
                                 # en la imagen

        self.folder = folder_input
        BaseMatriz.__init__(self, evento, fechas_anteriores, fechas_posteriores)


    def _CodificacionNombreArchivos(self):
        """Especifica dónde se encuentran las imágenes raster
        """
        self._fec_ini = 8 # la fecha comienza a partir de este caractér

        BaseMatriz._CodificacionNombreArchivos(self)

    def GraficarDatos(self, nro_imagen = 0):
        """Grafica los datos del evento

        nro_imagen: número de la imagen a graficar, desde 1 hasta N.
        N depende de fechas_anteriores, fechas_posteriores y las imágenes
        propias del evento.
        """
#        print self._archivos_destacados
        if nro_imagen == 0:
            for indice_img in range(len(self._archivos_destacados)):
                plot = BaseMatriz.GraficarDatos(self, indice_img + 1)

                plot.subplot(121)
                plot.title("Temperatura de la Superficie Terrestre")

                plot.show()
        else:
            plot = BaseMatriz.GraficarDatos(self, nro_imagen)

            plot.subplot(121)
            plot.title("Temperatura de la Superficie Terrestre")

            plot.show()

    def Estado(self):
        return self._estado

class MatrizMOD13(BaseMatriz):
    """Clase para manejo de información sobre índice NDVI obtenida de imágenes ERDAS IMAGINE
    """
    def __init__(self, evento, fechas_anteriores, fechas_posteriores, folder_input):
        """ Defino la configuración para estas imágenes raster
        """
        self.info = "NDVI"
        self.factor_escala = 0.0001
        self.offset = 0
        self.bins = 51           # Cantidad de bins del histograma
        self.absoluto = True     # Mapa de colores con rango absoluto o relativo
        self.colormap = 'RdYlGn' # Mapa de colores
        self.clim = [-1, 1]      # Impongo rango de valores mayor a los incluidos
                                 # en la imagen

        self.folder = folder_input
#        print self.folder
        BaseMatriz.__init__(self, evento, fechas_anteriores, fechas_posteriores)

    def _CodificacionNombreArchivos(self):
        """Especifica dónde se encuentran las imágenes raster
        """
        self._fec_ini = 8 # la fecha comienza a partir de este caractér

        BaseMatriz._CodificacionNombreArchivos(self)


    def GraficarDatos(self, nro_imagen = 0):
        """Grafica los datos del evento

        nro_imagen: número de la imagen a graficar, desde 1 hasta N.
        N depende de fechas_anteriores, fechas_posteriores y las imágenes
        propias del evento.
        """
#        print self._archivos_destacados
        if nro_imagen == 0:
            for indice_img in range(len(self._archivos_destacados)):
                plot = BaseMatriz.GraficarDatos(self, indice_img + 1)

                plot.subplot(121)
                plot.title("Indice de Vegetacion de Diferencia Normalizada\n(NDVI)")

                plot.show()
        else:
            plot = BaseMatriz.GraficarDatos(self, nro_imagen)

            plot.subplot(121)
            plot.title("Indice de Vegetacion de Diferencia Normalizada\n(NDVI)")

            plot.show()

    def Adjuntar_Riesgo(self):
        self._str_fecha_inicial
        self._matrizInterna
        for indice in range(1,len(self._matrizInterna["fecha"])):
            if (self._matrizInterna["fecha"][indice] == self._str_fecha_inicial):
#                print self._matrizInterna["fecha"][indice], self._str_fecha_inicial
                break

        titulo = []
        matrizRiesgo = []
        vectorRiesgo = []

        if(indice > 0):
            for indice2 in range(indice-1):
                for indice3 in range(len(self._matrizInterna["dato"][0])):
                    vectorRiesgo.append( \
                        self._matrizInterna["dato"][indice2][indice3] - \
                        self._matrizInterna["dato"][indice2+1][indice3])

                matrizRiesgo.append(vectorRiesgo)
                titulo.append("Riesgo " + str(self._matrizInterna["fecha"][indice2+1]))
                vectorRiesgo = []

            self._matrizInterna["dato"] +=  matrizRiesgo
            self._matrizInterna["fecha"] +=  titulo



    def Estado(self):
        return self._estado

class MatrizGLC2000(BaseMatriz):
    """Clase para manejo de información sobre índice MOD11 obtenida de imágenes Raster
    """
    def __init__(self, evento, fechas_anteriores, fechas_posteriores, folder_input):
        """ Defino el factor de escala y el offset para estas imágenes raster
        """
        self.info = "GLC2000"
        self.factor_escala = 1
        self.offset = 0
        self.bins = 22           # Cantidad de bins del histograma
        self.absoluto = True     # Mapa de colores con rango absoluto o relativo
        self.colormap = False    # Mapa de colores propio de la imagen

        self.folder = folder_input
        BaseMatriz.__init__(self, evento, fechas_anteriores, fechas_posteriores,self.info)


    def _CodificacionNombreArchivos(self):
        """Especifica dónde se encuentran las imágenes raster
        """
        self._fec_ini = 7 # la fecha comienza a partir de este caractér

        BaseMatriz._CodificacionNombreArchivos(self)

    def GraficarDatos(self, nro_imagen = 0):
        """Grafica los datos del evento

        nro_imagen: número de la imagen a graficar, desde 1 hasta N.
        N depende de fechas_anteriores, fechas_posteriores y las imágenes
        propias del evento.
        """

     ## Creacición de figure con el detalle del GLC2000
        directorio = self.folder + 'Descripcion_GLC2000.png'
        plt.figure(directorio)
        plt.suptitle(u"Descripción de clases de GLC2000", fontsize=14, fontweight='bold')
        img = mpimg.imread(directorio)
        plt.axis('off')
        plt.imshow(img)

     ## Proceso estandar
        plot = BaseMatriz.GraficarDatos(self, nro_imagen)

        plot.subplot(121)
        plot.title("Cobertura de la Superficie Global")

        plot.show()

    def Adjuntar_Traduccion(self):

        for indice in range(len(self._matrizInterna["fecha"])):
            if (self._matrizInterna["fecha"][indice] == "GLC2000"):
                break

        titulo = []

        vectorRiesgo = []
        matrizRiesgo = []
        cadena = "{0:^70}"

        for indice3 in range(len(self._matrizInterna["dato"][0])):
            self._matrizInterna["dato"][indice][indice3] = \
                    int(self._matrizInterna["dato"][indice][indice3])
            vectorRiesgo.append( \
                cadena.format(glc_dic[int(self._matrizInterna["dato"][indice][indice3] )]))

        matrizRiesgo.append(vectorRiesgo)

        titulo.append(cadena.format("Descripción"))

        self._matrizInterna["dato"] +=  matrizRiesgo
        self._matrizInterna["fecha"] +=  titulo

    def Estado(self):
        return self._estado



if __name__ == '__main__':
    import sys
    # import de módulos de directorios superiores
    root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    if not sys.path.count(root_dir):
        sys.path.insert(0, root_dir)
