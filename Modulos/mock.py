# -*- coding: utf-8 -*-
"""
Created on Thu Sep 22 13:41:55 2016

@author: Ivan
"""
import os, sys

# import de Clases.foco_y_evento y módulos de directorios superiores
root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if not sys.path.count(root_dir):
    sys.path.insert(0, root_dir)
from Clases.foco_y_evento import Foco, Evento



# Para calculo de sup de CABA
l_caba = []
l_caba1 = []
l_caba2 = []
A = 0
B = ((-54)+((-60)-(-54))/2) - ((-58.336149)+((-58.531433)-(-58.336149))/2)
print(B,-58.531433+B,-58.336149+B)
l_caba.append(Foco(-34.704714+A, -58.461173+B, 20160304, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_caba.append(Foco(-34.654316+A, -58.528979+B, 20160305, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_caba.append(Foco(-34.615488+A, -58.531433+B, 20160305, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_caba.append(Foco(-34.549444+A, -58.500510+B, 20160305, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_caba.append(Foco(-34.526802+A, -58.458087+B, 20160306, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_caba.append(Foco(-34.581434+A, -58.362233+B, 20160306, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))

l_caba.append(Foco(-34.627837+A, -58.336149+B, 20160306, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_caba.append(Foco(-34.644914+A, -58.358447+B, 20160307, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_caba.append(Foco(-34.661641+A, -58.394629+B, 20160307, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_caba.append(Foco(-34.659496+A, -58.417131+B, 20160308, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))

# Restar los siguientes:
l_caba1.append(Foco(-34.644914, -58.358447, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_caba1.append(Foco(-34.661641, -58.394629, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_caba1.append(Foco(-34.704714, -58.461173, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))

l_caba2.append(Foco(-34.661641, -58.394629, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_caba2.append(Foco(-34.659496, -58.417131, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_caba2.append(Foco(-34.704714, -58.461173, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))

l_focos2 = []

l_focos2.append(Foco(-28, -65, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos2.append(Foco(-28, -64, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos2.append(Foco(-27, -64, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos2.append(Foco(-27, -65, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos2.append(Foco(-26, -63, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos2.append(Foco(-29, -63, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))


l_focos = []

l_focos.append(Foco(-28.1, -65.165, 19981230, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.1, -65.1  , 19981230, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
#l_focos.append(Foco(-28.15, -65.1325  , 19981230, 2300, "Argentina", "MOCK", "MOCK"))



l_focos.append(Foco(-28.145, -65.158, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.1425,-65.156, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.140, -65.152, 19981231, 2300, "Argentina", "MOCK", "MOCK", "MOCK"))

# 3 nuevos
l_focos.append(Foco(-28.143, -65.154, 19990101, 1200, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.141, -65.151, 19990101, 1200, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.139, -65.150, 19990101, 1200, "Argentina", "MOCK", "MOCK", "MOCK"))

# 2 repetidos, 3 nuevos
l_focos.append(Foco(-28.141, -65.151, 19990101, 2030, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.139, -65.150, 19990101, 2030, "Argentina", "MOCK", "MOCK", "MOCK"))

l_focos.append(Foco(-28.138, -65.148, 19990101, 2030, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.136, -65.146, 19990101, 2030, "Argentina", "MOCK", "MOCK", "MOCK"))

# 1 repetidos, 4 nuevos, "MOCK"
l_focos.append(Foco(-28.136, -65.146, 19990102, 1100, "Argentina", "MOCK", "MOCK", "MOCK"))

l_focos.append(Foco(-28.134, -65.149, 19990102, 1100, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.135, -65.145, 19990102, 1100, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.132, -65.144, 19990102, 1100, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.130, -65.142, 19990102, 1100, "Argentina", "MOCK", "MOCK", "MOCK"))

# 4 nuevos
l_focos.append(Foco(-28.131, -65.148, 19990102, 2200, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.128, -65.146, 19990102, 2200, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.128, -65.142, 19990102, 2200, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.126, -65.140, 19990102, 2200, "Argentina", "MOCK", "MOCK", "MOCK"))

# 4 nuevos
l_focos.append(Foco(-28.129, -65.146, 19990103, 1010, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.126, -65.142, 19990103, 1010, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.125, -65.139, 19990103, 1010, "Argentina", "MOCK", "MOCK", "MOCK"))
l_focos.append(Foco(-28.124, -65.137, 19990103, 1010, "Argentina", "MOCK", "MOCK", "MOCK"))

####  Maipú provincia de Bs As

f_maipu = []
f_maipu.append(Foco(-36.772364, -57.772999, 20161204, 1200, "Argentina",  "Buenos Aires",  "MOCK", "MOCK"))
f_maipu.append(Foco(-36.765925, -57.762098, 20161204, 1200, "Argentina",  "Buenos Aires",  "MOCK", "MOCK"))
f_maipu.append(Foco(-36.767801, -57.736221, 20161204, 1200, "Argentina",  "Buenos Aires",  "MOCK", "MOCK"))
f_maipu.append(Foco(-36.790070, -57.722473, 20161204, 1200, "Argentina",  "Buenos Aires",  "MOCK", "MOCK"))
f_maipu.append(Foco(-36.789041, -57.756132, 20161204, 1200, "Argentina",  "Buenos Aires",  "MOCK", "MOCK"))
f_maipu.append(Foco(-36.781113, -57.752727, 20161204, 1200, "Argentina",  "Buenos Aires",  "MOCK", "MOCK"))
f_maipu.append(Foco(-36.795865, -57.745914, 20161204, 1200, "Argentina",  "Buenos Aires",  "MOCK", "MOCK"))
f_maipu.append(Foco(-36.772350, -57.728667, 20161204, 1200, "Argentina",  "Buenos Aires",  "MOCK", "MOCK"))
f_maipu.append(Foco(-36.787434, -57.726390, 20161204, 1200, "Argentina",  "Buenos Aires",  "MOCK", "MOCK"))


#### Evento real en San Luis
f_san_luis = []
f_san_luis.append(Foco(-33.85739, -66.11847, 20160824, 0306, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.85887, -66.12944, 20160824, 0306, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.86034, -66.14041, 20160824, 0306, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.85058, -66.12621, 20160824, 0306, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.85208, -66.13718, 20160824, 0306, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.85357, -66.14815, 20160824, 0306, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.85506, -66.15913, 20160824, 0306, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.89336, -66.13485, 20160824, 1823, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.89524, -66.14652, 20160824, 1823, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.88216, -66.12538, 20160824, 1823, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.88406, -66.13707, 20160824, 1823, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.88594, -66.14875, 20160824, 1823, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.87096, -66.11591, 20160824, 1823, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.87285, -66.12759, 20160824, 1823, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.87475, -66.1393,  20160824, 1823, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.86163, -66.11811, 20160824, 1823, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.86354, -66.12981, 20160824, 1823, "Argentina", "San Luis", "La Capital", "MOCK"))
f_san_luis.append(Foco(-33.86544, -66.14151, 20160824, 1823, "Argentina", "San Luis", "La Capital", "MOCK"))




############################# MOCK DE EVENTO ##################################

#Ev_MOCK = Evento(f_maipu, "MOCK")
Ev_MOCK = Evento(f_san_luis, "MOCK", 0)
