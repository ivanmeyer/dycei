#!/usr//env python
# -*- coding: utf-8 -*-

"""
Nombre de script:    raster.py
Objetivo:            Funciones para las imágenes raster
Autor:               Iván
"""

from definiciones import s

import os, sys
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import numpy as np
from numpy               import asarray
from matplotlib          import get_backend
from osgeo               import gdal, ogr, osr
from math                import floor, ceil
from matplotlib.colors   import LinearSegmentedColormap

gdal.UseExceptions()


#%%
########################################################################

"""A key capability of ERDAS_IMG is that it distinguishes between two types
of raster layer:

Continuous: A continuous raster layer typically represents an image captured
            by a sensor array comparable to a digital camera. Values may
            represent a single band or multiple bands. The value measured at
            each pixel may relate to any quantity measured in a continuous
            range, including aerial or satellite imagery, temperature,
            elevation, etc.

Thematic:   A thematic raster layer comprises pixels that have been classified,
*********   or put into distinct categories. Each pixel has a single value,
            which is simply a numeric code for a particular category. A "false"
            or "pseudo" color is used to represent each category for display.
            Since the category values are not necessarily related, gradations
            that are helpful in true color mode are not usually useful in
            pseudo color. The class system gives the thematic layer a discrete
            look, in which each class can have its own color."""

#%%
def Open(nombre_archivo):
    """Abre un archivo raster y muestra su metadata.
    Devuelve el dataset del archivo

    nombre_archivo: Nombre del archivo a abrir
    """
    try:
        dataset = gdal.Open(nombre_archivo)
    except:
        print "Error al intentar abrir el archivo"
        print nombre_archivo
    # Muestro el metadata en pantalla, si lo posee

    metadata = dataset.GetMetadata()
    if metadata:
        print metadata
    else:
        print "Metadata no existente"
    return dataset

#%%
def InfoBanda(dataset, num_banda):
    """Consulta en el dataset si existe la banda indicada y muestra su información

    dataset: acceso al archivo raster

    num_banda: numero de la banda a consultar

    Devuelve:
        minimo: valor mínimo de la banda en formato float

        maximo: valor máximo de la banda en formato float
    """
    try:
        banda = dataset.GetRasterBand(num_banda)
    except RuntimeError, e:
        print "Banda número %i no existente" % num_banda
        print e
        sys.exit(1)

    minimo = banda.GetMinimum()
    maximo = banda.GetMaximum()

    # Para cuando no está definido el mínimo ni el máximo. Ej en Tiff
    if not minimo:
        arr = banda.ReadAsArray()
        [col, rows] = arr.shape
        minimo = arr.min()
        maximo = arr.max()

    # Muestra información de la banda de la imagen raster

    print "[ NO DATA VALUE ] = ", banda.GetNoDataValue()
    print "[ OFFSET ] = ", banda.GetOffset()
    print "[ MIN ] = %i" % minimo
    print "[ MAX ] = %i" % maximo
    print "[ SCALE ] = ", banda.GetScale()
    print "[ OFFSET ] = ", banda.GetOffset()


    unidad = banda.GetUnitType()
    if unidad:
        print "[ UNIT TYPE ] = %s" % unidad


    color_table = banda.GetColorTable()
    if color_table is None:
        print "ColorTable no existente"
    else:
        print "[ COLOR TABLE COUNT ] = %i" % color_table.GetCount()

     ## Si la imagen tiene un Color Table, lo obtengo a partir de la imagen
        print "Se imprime Color Table propio del Tiff, sin repeticiones:"
        colors = []

        cantidad = int(maximo)-int(minimo)+1
        for i in range( int(minimo), int(maximo)+1):
            entry = color_table.GetColorEntry( i )
            if not entry:
                continue
            else:
                print entry
                # Creo el Color Map
                colors.append((int(entry[0])/255.,int(entry[1])/255.,int(entry[2])/255.))
                #print "[ COLOR ENTRY RGB ] = ", color_table.GetColorEntryAsRGB(i, entry)

        cmap = LinearSegmentedColormap.from_list("my_colormap",colors, N=cantidad, gamma=1.0)
        return minimo, maximo, cmap

    return minimo, maximo, False


def CopiarZona(coordenadas, dataset, y_focos=[], x_focos=[], EPSG_origen=4326):
    """Devuelve una zona de un raster. Opcionalmente puede indicar focos
    dentro de la zona

    coordenadas: coordenadas inferiores y superiores como lista
                 [LatInf, LatSup, LonIzq, LonDer]

    dataset: acceso al archivo raster de origen

    y_focos: lista de valores 'y' de los focos a indicar (ej latitudes)
             Predeterminado: lista vacía

    x_focos: lista de valores 'x' de los focos a indicar (ej longitudes)
             Predeterminado: lista vacía

    EPSG_origen: referencia espacial de origen. Predeterminado = 4326
    """
    if type(x_focos) != list or type(y_focos) != list:
        raise ValueError("'x_focos' e 'y_focos' deben ser ingresados como listas")

    if len(x_focos) != len(y_focos):
        raise ValueError("La dimension de las listas 'x_focos' e 'y_focos' " \
                         "deben ser iguales")

    [y_inf, y_sup, x_izq, x_der] = coordenadas

    # Consulto la proyección de origen, ingresada por usuario
    ref_origen = osr.SpatialReference()
    ref_origen.ImportFromEPSG(EPSG_origen)

    # Consulto la proyección de destino, definida por el raster
    ref_destino = osr.SpatialReference(wkt=dataset.GetProjection())

    # Creo dos puntos con las 'coordenadas' para poder transformar
    [x_der, x_izq], [y_sup, y_inf] = TransformarCoordenadas(ref_origen, \
                                            ref_destino, \
                                            [x_der, x_izq], [y_sup, y_inf] )

    """ GetGeoTransform() devueve geo_transf. Para una imagen se cumple:
     _______________________________________________________________________
    |origen = (geo_transf[0], geo_transf[3])
    |
    |
    |                     tamaño de PIXEL
    |                     (rectangular):
    |                       __________
    |                      |          |
    |                      |          | geo_transf[5]
    |                      |__________|
    |                      geo_transf[1]
    |
    """
    geo_transf = dataset.GetGeoTransform()

    # Cálculo del pixel para el valor x_izq e y_sup
    x_izq_pixel = floor((x_izq - geo_transf[0]) / geo_transf[1]) # x del pixel
    y_sup_pixel = floor((y_sup - geo_transf[3]) / geo_transf[5]) # y del pixel

    # Cálculo del pixel para el valor x_der e y_inf
    x_der_pixel = ceil((x_der - geo_transf[0]) / geo_transf[1]) # x del pixel
    y_inf_pixel = ceil((y_inf - geo_transf[3]) / geo_transf[5]) # y del pixel

    # Dimensiones calculadas con valores enteros
    ancho = x_der_pixel - x_izq_pixel
    alto  = y_inf_pixel - y_sup_pixel

    # Creo listas vacías para los focos a graficar
    pix_foco_x = []
    pix_foco_y = []

    # Si hay focos para graficar, los transformo
    if x_focos != []:
        # Genero una lista 'puntos' formada por listas 'x_focos' e 'y_focos'
        puntos = list( TransformarCoordenadas(ref_origen,  \
                                              ref_destino, \
                                              x_focos, y_focos ) )

        # Busco los pixeles para cada item de la lista 'puntos', o sea para cada foco
        for i in range(len(puntos[0])):
            pix_foco_x.append((puntos[0][i] - x_izq ) / geo_transf[1]) # x del pixel
            pix_foco_y.append((puntos[1][i] - y_sup ) / geo_transf[5]) # y del pixel

    banda = dataset.GetRasterBand(1)
    # las unidades son 'número de pixel'

    return (asarray(banda.ReadAsArray(x_izq_pixel, y_sup_pixel, ancho, alto)), \
                         [pix_foco_x,  pix_foco_y])


def TransformarCoordenadas(ref_origen, ref_destino, x, y ):
    """Realiza la transformación de coordenadas entre dos referencias espaciales.

    ref_origen: osr.SpatialReference() de origen

    ref_destino: osr.SpatialReference() de destino

    x = lista de valores 'x' a transformar (ej longitudes)

    y = lista de valores 'y' a transformar (ej latitudes)

    Devuelve:

        ([ X1, X2, X3, ... ],
         [ Y1, Y2, Y3, ... ])
    """
    if type(x) != list or type(y) != list:
        raise ValueError("Los valores 'x' e 'y' deben ser ingresados como listas")
    if len(x) != len(y):
        raise ValueError("La dimensión de las listas 'x' e 'y' deben ser iguales")

    # Si las proyecciones son iguales, devuelvo las mismas listas
    if ref_origen.IsSame(ref_destino) == True:

        return (x, y)

    # Si las proyecciones son distintas, hago la transformación
    else:
        # Creo la capa de puntos a transformar
        multipunto = ogr.Geometry(ogr.wkbMultiPoint)

        # Creo un punto geométrico para cada par (x, y) del argumento 'puntos'
        for i in range(len(x)):
            point = ogr.Geometry(ogr.wkbPoint)
            point.AddPoint(x[i], y[i])
            multipunto.AddGeometry(point)

        # Transformo las coordenadas de los puntos
        coord_transform = osr.CoordinateTransformation(ref_origen, ref_destino)
        multipunto.Transform(coord_transform)

        # Listas con los puntos transformados
        X_transf = []
        Y_transf = []

        # Cargo las listas con los valores obtenidos
        for i in range(len(x)):
            p = multipunto.GetGeometryRef(i)
            X_transf.append(p.GetX())
            Y_transf.append(p.GetY())

        return (X_transf, Y_transf)


def ConsultarDatos(nombre_archivo, evento, factor_escala=1.0, offset=0.0):
    """Abre un archivo raster y devuelve los datos consultados en el archivo

    nombre_archivo: Nmbre del archivo a abrir

    evento: evento de incendio con los focos a consultar

    factor_escala: factor de escala a aplicar a los índices. Predeterminado = 1.0

    offset: corrimiento a aplicar a los índices. Predeterminado = 0.0
    """
    # Abro el archivo
    dataset = Open(nombre_archivo)

    # Consulto por la banda número 1
    num_banda = 1
    InfoBanda(dataset, num_banda)

    # Busco los valores pedidos en la imagen raster
    recorte, [x, y] = CopiarZona(evento.Coord(), dataset, \
                                 *evento.UbicacionFocos())

    # Cierro el archivo
    dataset = None

    indices = [round( recorte[ int(y[i]) ][ int(x[i]) ] * factor_escala + offset, \
               4) for i in range(len(x))]

    return indices


def GraficarDatos(nombre_archivo, evento, \
                  factor_escala=1.0, offset=0.0, \
                  bins=51, absoluto=True, \
                  colormap='gist_earth', clim=[None, None]):
    """Abre un archivo raster y grafica los datos consultados en el archivo

    nombre_archivo: nombre del archivo a abrir

    evento: evento de incendio con los focos a graficar

    factor_escala: factor de escala a aplicar a los índices. Predeterminado = 1.0

    offset: corrimiento a aplicar a los índices. Predeterminado = 0.0

    bins: cantidad de bins en el histograma, por ejemplo para GLC2000 debe ser
          22. Predeterminado = 51

    absoluto: mapa de colores con rango absoluto o relativo. Predeterminado = True

    colormap: mapa de colores de Matplotlib o ruta a archivo de mapa de colores (.clr).
              Predeterminado = 'gist_earth'

    clim: límite impuesto para el mapa de colores. Predeterminado = [None, None]
    """
    # Abro el archivo
    dataset = Open(nombre_archivo)

    # Consulto por la banda número 1
    num_banda = 1

    val_min, val_max, cmap = InfoBanda(dataset, num_banda)

    # Busco los valores pedidos en la imagen raster
    recorte, [x, y] = CopiarZona(evento.Coord(), dataset,
                                 *evento.UbicacionFocos())

    # Cierro el archivo
    dataset = None

    # Corrijo el recorte con el factor de escala y el offset
    recorte_corr = [round(recorte[i][j] * factor_escala + offset, 4) \
                    # para cada una de las columnas
                    for i in range(len(recorte)) \
                    # para cada una de las filas
                    for j in range(len(recorte[0]))]

    fig = plt.figure(nombre_archivo + " - Evento: " + str(evento.id))

 ## Nombre de la imagen (con la fecha) dentro del figure
    archivo = nombre_archivo.split(s) #("/")
    archivo = archivo.pop()
    fig.suptitle(archivo, fontsize=14, fontweight='bold')

    ######################################################################
    # Muestro en el subplot 1 la zona de interés y los focos del evento

    subplt1 = plt.subplot(121)

    """No interpolar:   interpolation='nearest'
    Gama de colores:    cmap=plt.cm.gist_earth   opciones: Greens, hot, summer
    Aspecto escala 1:1: aspect='equal'

    http://matplotlib.org/examples/color/colormaps_reference.html
    """
 ## Selecciono el mapa de colores:
    # Primero, el que esté indicado en el Metadata
    if cmap == False:
        # Segundo, el indicado entre las opciones de Matplotlib
        try:
            cmap = plt.cm.get_cmap(colormap)

        # Tercero, el de la ruta indicada por el usuario
        except ValueError:
            cmap = LeerColorTable(colormap, val_min, val_max)

 ## Recorte de imagen Raster
    # Si se definió el rango de colores, se impone
    if clim != [None, None]:
            # Desafecto el rango por el factor de escala y el offset para trabajar
            # con los valores reales, que son los mostrados en subplot(121)
            clim2 = [(clim[i]- offset)/factor_escala for i in range(2)]
            # El rango de colores es el de la imagen completa
            plt.imshow(recorte, interpolation='nearest',\
                       cmap=cmap, aspect='equal',
                       clim=clim2)
    # Si no se definió, puede ser el de la imagen completa o el del recorte
    else:
        if absoluto == True:
            # El rango de colores es el de la imagen completa
            plt.imshow(recorte, interpolation='nearest',\
                       cmap=cmap, aspect='equal',
                       clim=(val_min, val_max))
        else:
            # El rango de colores es el del recorte
            plt.imshow(recorte, interpolation='nearest',\
                       cmap=cmap, aspect='equal')

 ## Ejes en grados, lat y lon
    subplt1.yaxis.set_label_text( str(evento.Coord()[0:2]) + u" °")
    subplt1.xaxis.set_label_text( str(evento.Coord()[2:4]) + u" °")

 ## Colorbar
    # El colorbar utiliza una escala diferente al Recorte, porque usa valores
    # correjidos por el factor y el offset

    # Rango correjido
    rango_min = val_min * factor_escala + offset
    rango_max = val_max * factor_escala + offset

    print "rango_min", rango_min
    print "rango_max", rango_max

    # Configuro un Scalar Mappable solo para el colorbar (es un hack)

    # Si se definió el rango de colores, se impone
    if clim != [None, None]:
            my_norm = plt.Normalize(vmin=clim[0], vmax=clim[1])
    # Si no se definió, puede ser el de la imagen completa o el del recorte
    else:
        if absoluto == True:
            my_norm = plt.Normalize(vmin=rango_min, vmax=rango_max)
        else:
            # Colorbar únicamente con rango de la imagen
            im_min = min(recorte_corr)
            im_max = max(recorte_corr)

            my_norm = plt.Normalize(vmin=im_min, vmax=im_max)
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=my_norm)
    sm._A = []  # Elemento fantasma, solo para poder crear el Scalar Mappable
    plt.colorbar(sm)

 ## Muestro los focos sobre la imagen raster
    if x != []:
#        print u"Se grafican los focos añadidos en CopiarZona()"
        plt.autoscale(False)
        plt.plot( x, y, 'ro') # Agrego un punto rojo en el foco

        for i in range(len(x)):
            # Agrego el id del foco
            plt.text(x[i]+0.2, y[i], evento.Foco[i].id, color='white' )

    ######################################################################
    # Muestro en el subplot 2 el histograma de los valores raster
    subplt2 = plt.subplot(122)

    bins = int(bins)

 ## Histograma con los índices correjidos
    # Si se definió el rango de colores, se impone
    if clim != [None, None]:
        n, n_bins, patches = plt.hist(recorte_corr, bins=bins, normed=1, \
                                      range = clim)


    # Si no se definió, puede ser el de la imagen completa o el del recorte
    else:
        if absoluto == True:
            n, n_bins, patches = plt.hist(recorte_corr, bins=bins, normed=1, \
                                          range = (rango_min, rango_max))

            # Escala de colores a representar, de 0 a 1
            escala_color = asarray([i/1.0 for i in range(bins)])
            escala_color /= bins

            # Asigno color a cada bin del histograma
            for c, p in zip(escala_color, patches):
                plt.setp(p, 'facecolor', cmap(c))
        else:
            n, n_bins, patches = plt.hist(recorte_corr, bins=bins, normed=1, \
                                          range = (im_min, im_max))

            # Escala de colores a representar, de 0 a 1
            escala_color = asarray([i/1.0 for i in range(bins)])
            escala_color /= bins

            # Asigno color a cada bin del histograma
            for c, p in zip(escala_color, patches):
                plt.setp(p, 'facecolor', cmap(c))


 ## Campana con media y desvio
    # Cálculo de valores estadísticos
    mu = np.mean(recorte_corr)
    median = np.median(recorte_corr)
    sigma = np.std(recorte_corr)

    # Representación gráfica de la densidad de probabilidad
    y = mlab.normpdf(n_bins, mu, sigma)
    subplt2.plot(n_bins, y, 'r--', linewidth=2)

    # Texto con valores estadísticos
    textstr = '$\mu=%.4f$\n$\sigma=%.4f$\n$\mathrm{mediana}=%.4f$'%(mu, sigma, median)
    # Propiedades del recuadro de texto
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    subplt2.text(0.05, 0.95, textstr, transform=subplt2.transAxes, fontsize=14,
        verticalalignment='top', bbox=props)


 ## Representación de título
    subplt2.set_xlabel(u'Índices')
    subplt2.set_ylabel('Densidad de probabilidad')
    subplt2.set_title(u'Histograma de la zona de interés')

    ######################################################################
    # Maximizo el figure
    if (get_backend() == u'Qt'):
        print 'Backend: Qt'
        figManager = plt.get_current_fig_manager()
        figManager.window.showMaximized()
    elif(get_backend() == u'TkAgg'):
        print'Backend: TkAgg'
        figManager = plt.get_current_fig_manager()
        figManager.window.state('zoomed')
    elif(get_backend() == u'Qt4Agg'):
        print 'Backend: Qt4Agg'
        figManager = plt.get_current_fig_manager()
        figManager.window.showMaximized()
    else:
        print 'No se pudo maximizar el figure de Matplotlib'
        print 'Backend: ', get_backend()

    return plt


def LeerColorTable(archivo_clr, minimo=0, maximo=0):
    """Función para leer archivos de mapa de colores (.clr)

    minimo: primer elemento del mapa de colores. Predeterminado: 0

    maximo: elemento límite del mapa de colores, útil para mapas con repeticiones.
    Predeterminado: 0 (sin límite)
    """
    maximo = int(maximo)
    colors = []
    levels = []
    if os.path.exists(archivo_clr) is False:
        raise Exception("El archivo de mapa de colores " + archivo_clr + " no existe.")

    fp = open(archivo_clr, "r")
    for line_number, line in enumerate(fp):
        # Si el color está definido fuera del rango, lo evito
        if line_number < minimo:
            continue
        # Si se fijó un máximo y ya se alcanzó, sale
        if maximo != 0 and line_number > maximo:
            break

        # De lo contrario crea el color
        if line.find('#') == -1 and line.find('/') == -1:
            entry = line.split()
            levels.append(eval(entry[0]))
            colors.append((int(entry[1])/255.,int(entry[2])/255.,int(entry[3])/255.))

    fp.close()

    cmap = LinearSegmentedColormap.from_list("my_colormap",colors, N=len(levels), gamma=1.0)

#    return levels, cmap
    return cmap

#%%

### Funciones para terminar a futuro relacionadas con la creación de archivos SHP

def GenerarPoligono(num_banda, dataset, archivo_salida):
    """Genera un conjunto de archivos Shapefile con un poligono formado por
    la banda ingresada

    num_banda: Número de la banda a graficar

    dataset: Acceso al archivo raster de origen

    archivo_salida: Nombre del archivo Shapefile de salida
    """
    drv = ogr.GetDriverByName("ESRI Shapefile")
    dst_datasource = drv.CreateDataSource(archivo_salida + ".shp" )
    dst_layer = dst_datasource.CreateLayer(archivo_salida, srs = None )

    banda = dataset.GetRasterBand(num_banda)
    gdal.Polygonize( banda, None, dst_layer, -1, [], callback=None )


#TODO  Para terminar si es conveniente
#def array2shp(array,outSHPfn,rasterfn,pixelValue):
#    """https://pcjericks.github.io/py-gdalogr-cookbook/raster_layers.html"""
#
#    # max distance between points
#    raster = gdal.Open(rasterfn)
#    geotransform = raster.GetGeoTransform()
#    pixelWidth = geotransform[1]
#    maxDistance = ceil(sqrt(2*pixelWidth*pixelWidth))
#    print maxDistance
#
#    # array2dict
#    count = 0
#    roadList = np.where(array == pixelValue)
#    multipoint = ogr.Geometry(ogr.wkbMultiLineString)
#    pointDict = {}
#    for indexY in roadList[0]:
#        indexX = roadList[1][count]
#        Xcoord, Ycoord = pixelOffset2coord(rasterfn,indexX,indexY)
#        pointDict[count] = (Xcoord, Ycoord)
#        count += 1
#
#    # dict2wkbMultiLineString
#    multiline = ogr.Geometry(ogr.wkbMultiLineString)
#    for i in itertools.combinations(pointDict.values(), 2):
#        point1 = ogr.Geometry(ogr.wkbPoint)
#        point1.AddPoint(i[0][0],i[0][1])
#        point2 = ogr.Geometry(ogr.wkbPoint)
#        point2.AddPoint(i[1][0],i[1][1])
#
#        distance = point1.Distance(point2)
#
#        if distance < maxDistance:
#            line = ogr.Geometry(ogr.wkbLineString)
#            line.AddPoint(i[0][0],i[0][1])
#            line.AddPoint(i[1][0],i[1][1])
#            multiline.AddGeometry(line)
#
#    # wkbMultiLineString2shp
#    shpDriver = ogr.GetDriverByName("ESRI Shapefile")
#    if os.path.exists(outSHPfn):
#        shpDriver.DeleteDataSource(outSHPfn)
#    outDataSource = shpDriver.CreateDataSource(outSHPfn)
#    outLayer = outDataSource.CreateLayer(outSHPfn, geom_type=ogr.wkbMultiLineString )
#    featureDefn = outLayer.GetLayerDefn()
#    outFeature = ogr.Feature(featureDefn)
#    outFeature.SetGeometry(multiline)
#    outLayer.CreateFeature(outFeature)


#%%
# Código de prueba

if __name__ == '__main__':
    from mock import Ev_MOCK

    print "Se abre una imagen Raster"

    root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

###############################################################################
#    file_name = "Entradas\Rasters\\GLC2000\GLC2000_Sudamerica.tif"
    file_name = "Entradas\Rasters\MOD11\MOD11A2.2016225.LST_Day_1km.tif"
#    file_name = "Entradas\Rasters\\MOD13\MOD13A2.2016273.1_km_16_days_NDVI.tif"

    """ Para MOD11:"""
    factor_escala = 0.02
    offset = -273.15

    """ Para MOD13:"""
#    factor_escala = 0.0001
#    offset = 0

    """ Para GLC2000:"""
#    factor_escala = 1
#    offset = 0
###############################################################################

    file_name = root_dir + s + file_name
    if os.path.exists(file_name):
        print "Exite directorio"
    else:
        print "no existe directorio:", file_name

    #archivo_salida = "salida"
    #GenerarPoligono(num_banda, dataset, archivo_salida)

    # Muestro en el subplot 1 una zona de la imagen raster
#    recorte, [x, y] = CopiarZona([-39.5, -38.67, -63, -61.7], dataset )

#    recorte, [x, y] = CopiarZona([-39.5, -38.67, -63, -61.7], dataset, \
#                                [-39.11, -39.2], [-62.22, -62.5])

    consulta = ConsultarDatos(file_name, Ev_MOCK, factor_escala, offset)

    print "La consulta retornó: "
    print consulta

    plot = GraficarDatos(file_name, Ev_MOCK, factor_escala, offset,\
#               23, True, 'D:\Repos_GIT\DyCEI\Raster\geotiff\GLC2000\GLC2000_Sudamerica.clr')
               76, True, 'rainbow', [10,55]) # 'gist_ncar' # Para MOD11
#               51, False, 'bwr') # 'gist_ncar' # Para MOD11

#               51, True, 'RdYlGn', [-1, 1])  # Para MOD13

#                22, True)  # Para GLC2000

    plot.subplot(121)
    plot.title("Indice de Vegetacion de Diferencia Normalizada\n(NDVI)")

    plot.show()
    #plt.savefig("figure.png")
