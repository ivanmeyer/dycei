#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Nombre de script:    plotter.py
Objetivo:            Generar varios tipos de gráficos
Autor:               Iván - Daniel
"""
#from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np


def PlotFocos(lista_focos, titulo = "PlotFocos()", figure_num = None,
              color_relleno = "r", color_borde = "k"):
    """Grafica una lista de focos.

    lista_focos: la lista de focos a graficar

    titulo: titulo del figure. Predeterminado = "PlotFocos()"

    figure_num: número del figure a graficar.
        Si no se provee o no existe el figure, se crea el figure.
        Si existe el figure, se superpone al gráfico existente.
        Ver figure de matplotlib.pyplot para más información

    color_relleno: el color interno del foco. Predeterminado = rojo

    color_borde: el color de contorno del foco. Predeterminado = negro
    """

##    plt.figure( num=figure_num, figsize=(10,10) )
#    my_map = Basemap(projection='merc', lat_0 = -38, lon_0 = -60,
#                     resolution = 'l', area_thresh = 10000, #0.1,
#                     llcrnrlon=-75, urcrnrlon=-53,
#                     llcrnrlat=-56, urcrnrlat=-21)
#
##    my_map.shadedrelief()
#
#    my_map.drawcoastlines()
#    my_map.drawcountries()
#    my_map.drawstates()
#    my_map.drawmapboundary(fill_color='#99ffff')
#    my_map.fillcontinents(color='#cc9966',lake_color='#99ffff')
#
#    lats = [lista_focos[i].Lat() for i in range(len(lista_focos))]
#    lons = [lista_focos[i].Lon() for i in range(len(lista_focos))]
#
#    x,y = my_map(lons, lats)
#    my_map.plot(x, y, "o", ms=5, mew=1, mfc=color_relleno, mec=color_borde)
#    plt.suptitle(titulo)
#    plt.show()
    pass


def PlotEventos(lista_eventos, titulo = "PlotEventos()", indicar_id = True, directorio = "no data"):
    """Grafica una lista de eventos de incendio.

    lista_eventos: la lista de eventos a graficar

    titulo: titulo del figure. Predeterminado = "PlotEventos()"

    indicar_id: opción de indicar los id de los focos. Predeterminado = True

    directorio: directorio donde guardar la imagen, o de lo contrario, mostrarla.
    Predeterminado = Mostrar imagen
    """
    plt.figure(titulo)
#    my_map = Basemap(projection='merc', lat_0 = -38, lon_0 = -60,
#                     resolution = 'l', area_thresh = 10000, #0.1,
#                     llcrnrlon=-75, urcrnrlon=-53,
#                     llcrnrlat=-56, urcrnrlat=-21)
#
##    my_map.shadedrelief()
#
#    my_map.drawcoastlines()
#    my_map.drawcountries()
#    my_map.drawstates()
#    my_map.drawmapboundary(fill_color='#99ffff')
#    my_map.fillcontinents(color='#cc9966',lake_color='#99ffff')
#
#    color_evento = [np.random.rand(len(lista_eventos))*int('0xffffff',16),\
#                    np.random.rand(len(lista_eventos))*int('0xffffff',16)]
#
#    color_centro   = [('#'+(hex(color_evento[0][i])[2:][:-1]).zfill(6)) for i in range(len (color_evento[0]))]
#    color_contorno = [('#'+(hex(color_evento[1][i])[2:][:-1]).zfill(6)) for i in range(len (color_evento[1]))]
#
#
#    for j in range(len(lista_eventos)):
#        lats, lons = lista_eventos[j].UbicacionFocos()
#
#        x,y = my_map(lons, lats)
#        my_map.plot(x, y, "o", ms=5, mew=1, mfc=color_centro[j], mec=color_contorno[j])
#
#        # Agrego id del foco
#        if indicar_id == True:
#            for i in range(lista_eventos[j].CantFocos()):
#                id_foco = lista_eventos[j].Foco[i].id
#
#                plt.text(x[i]+0.0015, y[i]+0.0015, \
#                         id_foco, fontsize=8)
#
#    plt.suptitle(titulo)
#
#    if(directorio == "no data"):
#        plt.show()
#    else:
#        plt.savefig(directorio)


def PlotPropagacion(lista_eventos, titulo = "PlotEventos()", indicar_id = True, directorio = "no data"):
    """Grafica los eventos de incendio con un degradé de color para su sentido
    de propagación. Blanco indica fecha incial (o evento de un solo día)

    lista_eventos: la lista de eventos a graficar

    titulo: titulo del figure. Predeterminado = "PlotEventos()"

    indicar_id: opción de indicar los id de los focos. Predeterminado = True

    directorio: directorio donde guardar la imagen, o de lo contrario, mostrarla.
    Predeterminado = Mostrar imagen
    """

#    plt.figure(titulo)
#    my_map = Basemap(projection='merc', lat_0 = -38, lon_0 = -60,
#                     resolution = 'l', area_thresh = 10000, #0.1,
#                     llcrnrlon=-75, urcrnrlon=-53,
#                     llcrnrlat=-56, urcrnrlat=-21)
#
##    my_map.shadedrelief()
#
#    my_map.drawcoastlines()
#    my_map.drawcountries()
#    my_map.drawstates()
#    my_map.drawmapboundary(fill_color='#99ffff')
#    my_map.fillcontinents(color='#cc9966',lake_color='#99ffff')
#
#    color_evento = [np.random.rand(len(lista_eventos))*int('0xffffff',16),\
#                    np.random.rand(len(lista_eventos))*int('0xffffff',16)]
#
#
#    color_centro   = [('#'+(hex(color_evento[0][i])[2:][:-1]).zfill(6)) for i in range(len (color_evento[0]))]
#    color_contorno = [('#'+(hex(color_evento[1][i])[2:][:-1]).zfill(6)) for i in range(len (color_evento[1]))]
#
#
#    ############  NUEVO CÓDIGO
#
#    color_centro = np.random.rand(len(lista_eventos), 2)
#
#
#
#    for j in range(len(lista_eventos)):
#        lats, lons = lista_eventos[j].UbicacionFocos()
#        cant_focos = lista_eventos[j].CantFocos()
#        fecha_inicio_evento = lista_eventos[j].PrintFecIni(0)
#        duracion_evento = lista_eventos[j].Duracion()
#
#        x, y = my_map(lons, lats)
#
#
#        color_centro_num = [(256/(duracion_evento-1))*k-1 for k in range(1,duracion_evento)]
#        color_centro_num = [0] + color_centro_num
#        # Creo gama de colores para el evento
#
#
#        for i in range(cant_focos):
#            fecha_foco = lista_eventos[j].Foco[i].Fecha(1)
#            df = fecha_foco - fecha_inicio_evento
#
#            color_centro = ('#' + "FF" \
#                        + (hex(255-color_centro_num[df.days])[2:]).zfill(2) \
#                        + (hex(255-color_centro_num[df.days])[2:]).zfill(2))
#
#            my_map.plot(x[i], y[i], "o", ms=5, mew=1, mfc=color_centro, mec=color_contorno[j])
#
#        # Agrego id del foco
#        if indicar_id == True:
#            for i in range(lista_eventos[j].CantFocos()):
#                id_foco = lista_eventos[j].Foco[i].id
#
#                plt.text(x[i]+0.0015, y[i]+0.0015, \
#                         id_foco, fontsize=8)
#
#    plt.suptitle(titulo)
#
#    if(directorio == "no data"):
#        plt.show()
#    else:
#        plt.savefig(directorio)


def PlotMesh(lista_eventos):
    """Grafica la triangulación de Delaunay de una lista de evento de incendio

    lista_eventos: la lista de eventos a graficar
    """
    plt.figure(figsize = (15,15))

    for i in range(len(lista_eventos)):
        # radio_min = 0.25
        triang = lista_eventos[i]._Mesh()
        if triang is not None:
            plt.triplot(triang, 'bo-')

            # Agrego id del evento
            coord = lista_eventos[i].Coord()
            plt.text(coord[3], coord[1], lista_eventos[i].id)
#            print coord,lista_eventos[i].id
            try:
                plt.text(coord[3], coord[1], lista_eventos[i].id)
            except:
                pass
        else:
            print u"ATENCIÓN: El evento %d tiene todos los focos alineados. \
No se grafica en PlotMesh" %(i)
    plt.suptitle('Triplot de triangulaciones de Delaunay')
    plt.axis('equal')
    plt.grid()
    plt.show()
