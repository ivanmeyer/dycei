# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 16:04:29 2017

@author: dany-xl
"""

import sys
import os


###############################################################################
def sintax_for_os():
    # Find OS ans switch to correct path notation
    if sys.platform == "win32":
        slash = '\\'
    elif sys.platform == "linux" or sys.platform == "linux2":
        slash = '/'
    return(slash)

###############################################################################
s = sintax_for_os()

dir_biblioteca  = os.getcwd() + s + "Biblioteca" + s
dir_iconos      = dir_biblioteca+"Iconos"+s
dir_reservas    = dir_biblioteca+"Shp_reservas"+s
dir_provincias  = dir_biblioteca+"Shp_provincias"+s+"provincias_argentinas.shp"
dir_localidades = dir_biblioteca+"Shp_localidades"+s+"localidades.shp"

dir_entradas    = os.getcwd() + s + "Entradas" + s
dir_testeo_rap  = dir_entradas+"Focos"+s+"mod14_sepa"+s+"SEPA_0818_a_0828_sanLuis.txt"
dir_rasters     = dir_entradas + "Rasters" + s
dir_mod11       = dir_rasters + "MOD11" + s
dir_mod13       = dir_rasters + "MOD13" + s
dir_glc2000     = dir_rasters + "GLC2000" + s

dir_manual      = os.getcwd() + s + "Manual_de_Usuario_-DyCEI-_v1.1.pdf"

dir_salidas     = os.getcwd() + s + "Salidas" + s

###############################################################################

txt_inicio = u'Bienvenido. Para comenzar abra un archivo CSV, TXT o SHP'
txt_open_sepa = 'Abrir archivo SEPA fomato CSV o TXT'
txt_open_modis = 'Abrir archivo MODIS fomato CSV o TXT'
txt_open_modis2 = 'Abrir archivo SEPA fomato SHP'

txt_licencia = u"""
   Derechos de autor 2017 Daniel Alejandro Martelliti e Iván Meyer.

   Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); \
se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia.

   Podrá obtener una copia de la Licencia en:

       http://www.apache.org/licenses/LICENSE-2.0

   A menos que lo exijan las leyes pertinentes o se haya establecido por \
escrito, el software distribuido en virtud de la Licencia se distribuye “TAL \
CUAL”, SIN GARANTÍAS NI CONDICIONES DE NINGÚN TIPO, ya sean expresas o \
implícitas.
   Véase la Licencia para consultar el texto específico relativo a los \
permisos y limitaciones establecidos en la Licencia.

"""
txt_autores = u"""
Contacto:

Daniel Alejandro Martelliti:
	danielmartelliti@yahoo.com.ar
Iván Meyer:
	ivan.meyer00@gmail.com

Buenos Aires, Argentina
"""

###############################################################################

tipos_guardar = [u"Documentación","Shapefile"]
tipos_guardar_especial = [u"Análisis Raster"]
tipos_raster = ["MOD11","MOD13 (con Riesgo)","GLC2000"]
tipos_ploteos = ["PlotEventos", u"Propagación", "PlotMesh"]
tipos_contorno = [u"Reservas Naturales","Rutas y Departamentos","Localidades"]

formato_sepa = ['Lat', 'Lon', 'Lugar', 'Cobertura', 'ANP', 'FRP', 'Fecha',
                'Sat', 'Hora']
formato_modis = ['latitude', 'longitude', 'brightness', 'scan', 'track',
                 'acq_date', 'acq_time', 'satellite', 'confidence', 'version',
                 'bright_t31', 'frp', 'daynight']


###############################################################################

provincias = ['Jujuy','Salta','Santiago del Estero',u'Tucumán','La Rioja',\
            'Catamarca', 'San Luis','Buenos Aires','Santa Fe', u'Córdoba',\
            'Tierra del Fuego','Misiones',u'Entre Ríos','Mendoza', 'Chaco',\
            'San Juan','La Pampa','Santa Cruz','Chubut',u'Neuquén',u'Río Negro',\
            'Corrientes','Formosa']

dir_rn_jujuy        = dir_reservas + "rn_jujuy.shp"
dir_rn_salta        = dir_reservas + "rn_salta.shp"
dir_rn_santiago     = dir_reservas + "rn_santiago_del_estero.shp"
dir_rn_tucuman      = dir_reservas + "rn_tucuman.shp"
dir_rn_larioja      = dir_reservas + "rn_la_rioja.shp"
dir_rn_catamarca    = dir_reservas + "rn_catamarca.shp"
dir_rn_sanluis      = dir_reservas + "rn_san_luis.shp"
dir_rn_buenosaires  = dir_reservas + "rn_buenos_aires.shp"
dir_rn_santafe      = dir_reservas + "rn_santa_fe.shp"
dir_rn_cordoba      = dir_reservas + "rn_cordoba.shp"
dir_rn_tfuego       = dir_reservas + "rn_tierra_del_fuego.shp"
dir_rn_misiones     = dir_reservas + "rn_misiones.shp"
dir_rn_entrerios    = dir_reservas + "rn_entre_rios.shp"
dir_rn_mendoza      = dir_reservas + "rn_mendoza.shp"
dir_rn_chaco        = dir_reservas + "rn_chaco.shp"
dir_rn_sanjuan      = dir_reservas + "rn_san_juan.shp"
dir_rn_lapampa      = dir_reservas + "rn_la_pampa.shp"
dir_rn_santacruz    = dir_reservas + "rn_santa_cruz.shp"
dir_rn_chubut       = dir_reservas + "rn_chubut.shp"
dir_rn_neuquen      = dir_reservas + "rn_neuquen.shp"
dir_rn_rionegro     = dir_reservas + "rn_rio_negro.shp"
dir_rn_corrientes   = dir_reservas + "rn_corrientes.shp"
dir_rn_formosa      = dir_reservas + "rn_formosa.shp"

directorios_shp = {'Jujuy':dir_rn_jujuy,\
                   'Salta':dir_rn_salta,\
                   'Santiago del Estero':dir_rn_santiago,\
                   'Tucuman':dir_rn_tucuman,\
                   'La Rioja':dir_rn_larioja,\
                   'Catamarca':dir_rn_catamarca,\
                   'San Luis':dir_rn_sanluis,\
                   'Buenos Aires':dir_rn_buenosaires,\
                   'Santa Fe':dir_rn_santafe,\
                   'Cordoba':dir_rn_cordoba,\
                   'Tierra del Fuego':dir_rn_tfuego,\
                   'Misiones':dir_rn_misiones,\
                   'Entre Rios':dir_rn_entrerios,\
                   'Mendoza':dir_rn_mendoza,\
                   'Chaco':dir_rn_chaco,\
                   'San Juan':dir_rn_sanjuan,\
                   'La Pampa':dir_rn_lapampa,\
                   'Santa Cruz':dir_rn_santacruz,\
                   'Chubut':dir_rn_chubut,\
                   'Neuquen':dir_rn_neuquen,\
                   'Rio Negro':dir_rn_rionegro,\
                   'Corrientes':dir_rn_corrientes,\
                   'Formosa':dir_rn_formosa}


###############################################################################

norma_adoptada  ={1:'Lista normalizada, formato: [ lat , long ].',\
                  2:'Lista normalizada, formato: [ lat , long , FechaHora ].',\
                  3:'Lista normalizada, formato: [ lat , long , Año , Mes , '\
                     + 'Día , Hora , Minutos ].'}

evento     =  '---Evento_id---'
focos      =  '-----Focos-----'
fechaInic  =  '-Fecha_Inicial-'
fechaFin   =  '--Fecha_Final--'
area       =  '---Área-(ha)---'
perimetro  =  '-Perímetro-(km)'
duracion   =  'Duración-(días)'
sentido    =  '----Sentido----'
velocidad  =  '---Vel-(m/d)---'

localidad  =  '---------------Localidades--------------'
reservas   =  '----------------Reservas Afectadas---------------'
rutas      =  '----------------------Rutas----------------------'

text_eventos = '|' + evento + '|' + focos + '|' + fechaInic + '|' + fechaFin + \
               '|' + area + '|' + perimetro + '|' + duracion + '|' + velocidad + '|' + sentido + \
               '|' + localidad + '|' + rutas + '|' + reservas + '\n'



foco       =  '----Foco_id----'
lat        =  '----Latitud----'
lon        =  '---Longitud----'
fecha      =  '-----Fecha-----'
prov       =  '---Provincia---'
pais       =  '-----País------'
depto      =  '---Departamentos----'
inicio_fin =  '---Inicial/Final----'

text_focos = '|' + foco +'|' + lat + '|' + lon + '|' + fecha + '|' + pais + \
             '|' + prov +'|' + depto + '|' + localidad + '|' + inicio_fin + '|' + rutas + \
             '|' + reservas + '\n'


focos_file   = 'focos'
eventos_file = 'eventos'


title_files_dict = {focos_file:text_focos,\
                    eventos_file:text_eventos}

provincias_keys = {'Jujuy':['Jujuy'],\
                   'Salta':['Salta'],\
                  'Santiago del Estero':['Santiago del Estero'],\
                  u'Tucumán':['Tucuman','Tucumn'],\
                  'La Rioja':['La Rioja'],\
                  'Catamarca':['Catamarca'],\
                  'San Luis':['San Luis'],\
                  'Buenos Aires':['Buenos Aires'],\
                  'Santa Fe':['Santa Fe'],\
                  u'Córdoba':['Cordoba'],\
                  'Tierra del Fuego':['Tierra del Fuego'],\
                  'Misiones':['Misiones'],\
                  u'Entre Ríos':['Entre Rios','Entre Ros'],\
                  'Mendoza':['Mendoza'],\
                  'Chaco':['Chaco'],\
                  'San Juan':['San Juan'],\
                  'La Pampa':['La Pampa'],\
                  'Santa Cruz':['Santa Cruz'],\
                  'Chubut':['Chubut'],\
                  u'Neuquén':['Neuquen'],\
                  u'Río Negro':['Rio Negro','Ro Negro'],\
                  'Corrientes':['Corrientes'],\
                  'Formosa':['Formosa']}

###############################################################################

glc_dic = {
    0:"Sin información",
    1:"Cobertura arbórea, hoja ancha, perenne",
    2:"Cobertura arbórea, hoja ancha, caducifolio, cerrado",
    3:"Cobertura arbórea, hoja ancha, caducifolio, abierto",
    4:"Cobertura arbórea, hoja acicular, perenne",
    5:"Cobertura arbórea, hoja acicular, caducifolia",
    6:"Cobertura arbórea, tipo mixto de hoja",
    7:"Cobertura arbórea, regularmente inundado, agua dulce",
    8:"Cobertura arbórea, regularmente inundado, agua salina",
    9:"Mosaico: Cobertura arbórea / otra vegetación natural",
   10:"Cobertura arbórea, quemado",
   11:"Cobertura arbustiva, cerrado-abierto, perenne",
   12:"Cobertura arbustiva, cerrado-abierto, caducifolio",
   13:"Cobertura herbácea, cerrado-abierto",
   14:"Cobertura herbácea dispersa o arbustiva dispersa",
   15:"Cobertura arbustiva regularmente inundada o cobertura herbácea",
   16:"Áreas cultivadas y manejadas",
   17:"Mosaico: Cultivo / cobertura arbórea / otra vagetación natural ",
   18:"Mosaico: Cultivo / cobertura arbustiva o herbácea",
   19:"Áreas desnudas",
   20:"Cuerpos de agua (Natural y artificial)",
   21:"Nivel y hielo (Natural y artificial)",
   22:"Superficies artificiales y áreas asociadas",
   23:"Sin información"
}


###############################################################################

##  Codigos de error
str_error = {
# Decodificador
 0:"Nota: Funcionando correctamente.",
 1:"Problema, archivo incorrecto.",
 2:"Problema, el directorio no existe.",
 3:"Problema, formato no compatible.",
 4:"Problema, no se pudo decodificar el archivo.",
 5:"Problema, el archivo provincias_argentinas.shp no se pudo abrir.",
 6:"Problema, no se pudo decodificar el archivo - campo invalido.",
 7:"Problema, la fecha inicial no puede ser mayor que la final.",

# Filtros y norma
 8:"Problema, tipo de MOD14 desconocido.",
 9:"Nota: Algunos focos fueron descartados por falla en la lectura.",

# Deteccion de eventos
10:"Problema, los parametros ingresados deben ser numéricos.",
11:"Nota: Se debe seleccionar al menos una opción.",

# Análisis cercanias
12:"Problema, no se pudo finalizar correctamente el análisis de cercanías.",
13:"Nota: falta el nombre del proyecto. \nSe guardará como 'Proyecto_shp'.",

# Almacenamiento
14:"Problema, falla en la creación del directorio.",
15:"Problema, falla al crear archivos de documentación.",
16:"Problema, falla al crear el archivo Shapefile.",
17:"Nota: No se detectó la Referencia Espacial. \nSe utilizó EPSG=4326.",

# Análisis raster
18:"Nota: Se debe seleccionar al menos un evento, seleccionando la fila.",
19:"Nota: Se debe seleccionar al menos un tipo de Raster.",
20:"Nota: Los campos de Fechas deben completarse solo con números.",
21:"Nota: Los valores de los campos de Fechas deben ser mayores a 0.",

# Analisis de contorno
22:"Nota: En algunos focos no se identificaron rutas y/o deptos.",
23:"Nota: algunos datos no se pudieron obtener por falta de conexión a internet.",

#Familia Raster
30:"Problema, no se encontró ninguna imagen Raster GLC2000.",
31:"Problema, no se encontró ninguna imagen Raster MOD13.",
32:"Problema, no se encontró ninguna imagen Raster MOD11.",
33:"Nota: Precaución con las fechas - El evento transcurrió con \
anterioridad a las fechas de los Rasters (MOD11).",
34:"Nota: Precaución con las fechas - Faltan Rasters anteriores para \
cubrir el evento (MOD11).",
35:"Nota: Precaución con las fechas - Faltan Rasters anteriores para \
abarcar lo pedido (MOD11).",
36:"Nota: Precaución con las fechas - El evento transcurrió con \
posterioridad a las fechas de los Rasters (MOD11).",
37:"Nota: Precaución con las fechas - Faltan Rasters posteriores para \
cubrir el evento (MOD11).",
38:"Nota: Precaución con las fechas - Faltan Rasters posteriores para \
abarcar lo pedido (MOD11).",
39:"Nota: Precaución con las fechas - El evento transcurrió con \
anterioridad a las fechas de los Rasters (MOD13).",
40:"Nota: Precaución con las fechas - Faltan Rasters anteriores para \
cubrir el evento (MOD13).",
41:"Nota: Precaución con las fechas - Faltan Rasters anteriores para \
abarcar lo pedido (MOD13).",
42:"Nota: Precaución con las fechas - El evento transcurrió con \
posterioridad a las fechas de los Rasters (MOD13).",
43:"Nota: Precaución con las fechas - Faltan Rasters posteriores para \
cubrir el evento (MOD13).",
44:"Nota: Precaución con las fechas - Faltan Rasters posteriores para \
abarcar lo pedido (MOD13)."
}


str_help = {
"F0: abrir sepa"            :"Abrir archivo SEPA formato TXT o CSV",
"F0: abrir modis"           :"Abrir archivo MODIS formato TXT o CSV",
"F0: abrir modis shp"       :"Abrir archivo MODIS formato SHP",
"F0: abrir config"          :"Configuración",
"F0: abrir licencia"        :"Licencia",
"F0: test"                  :"Verificación Rápida",
"F0: abrir manual"          :"Abrir Manual de Usuario",
"F0: salir"                 :"Salir",

"F1: titulo"                :"Fase en la cual se filtrarán\nlos datos de entrada\n en formato TXT, CSV o SHP",
"F1: provincias"            :"Se debe seleccionar al menos\nuna provincia a filtrar",
"F1: fechas"                :"La fecha inicial debe ser\nmenor o igual a la fecha\nfinal",
"F1: boton filtrar"         :"Filtrar focos",
"F1: boton grafico general" :"Graficar focos",

"F2: titulo"                :"Fase en la cual se realizará el\nproceso de detección de los eventos\nen base a los focos ya filtrados",
"F2: criterios"             :"El usuario debe imponer un\ncriterio para la detección de eventos con\nparámetro espacial y temporal",
"F2: espacial"              :"Criterio espacial: se recomienda mayor a 1000 m",
"F2: temporal"              :"Criterio temporal: se recomienda mayor a 2 días",
"F2: procesar"              :"Procesamiento de los focos detectados\npara la detección de eventos.\nPuede tardar algunos minutos",
"F2: plot eventos"          :"Gráfico de eventos detectados",
"F2: plot propagacion"      :"Gráfico de propagación de los eventos",
"F2: plot triangulacion"    :"Gráfico de triangulación de los eventos detectados",
"F2: boton grafico"         :"Graficar eventos",

"F3: titulo"                :"Fase en la cual se realizarán análisis\na partir de imágenes Raster",
"F3: mod11"                 :"Raster: Temperatura de la superficie terrestre",
"F3: mod13"                 :"Raster: Índice de vegetación",
"F3: glc"                   :"Raster: Cobertura de la superficie global 2000",
"F3: fechas"                :"Cantidad de imágenes Raster a incluir antes\ny después del evento a analizar",
"F3: analizar"              :"Generación de las tablas Raster",
"F3: graficar"              :"Se graficará el resultado de todas\nlas imágenes Raster seleccionadas",

"F4: titulo"                :"Fase en la cual se analizarán las cercanías de cada foco",
"F4: analizar"              :"Analizar cercanías. Puede tardar varios minutos",

"F5: titulo"                :"Fase en la cual se guardan los resultados obtenidos",
"F5: documentacion"         :"Eventos detectados y focos relacionados,\nincluídas las características",
"F5: shape"                 :"Shapefile de los eventos",
"F5: raster"                :"Tablas Rasters generadas",
"F5: nombre"                :"Nombre del directorio donde se almacenarán los resultados",
"F5: Guardar"               :"Guardar resultados"
}

plot_dic = ["F2: plot eventos","F2: plot propagacion","F2: plot triangulacion"]
raster_dic = ["F3: mod11","F3: mod13","F3: glc"]
guardar_dic = ["F5: documentacion","F5: shape"]
guardar2_dic = ["F5: raster"]
