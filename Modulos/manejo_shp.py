#!/usr/bin/env python
#-*- coding: utf-8

"""
Nombre de script:    create_shp.py
Objetivo:            Crear nuevo archivo Shapefile
Autor:               Daniel
"""

# Importa modulos y establece el directorio de trabajo

import os, fiona, utm

from osgeo                      import ogr, osr
from shapely.geometry           import shape
from Modulos.definiciones       import *


localidades_punto = []
localidades_nombre = []
"""
###############################################################################
Definición del directorio de trabajo
###############################################################################
"""
def DefDirectorio():
    """Define el directorio de trabajo para los archivos de salida
    """
    # Indico el directorio de trabajo actual
    retpath = os.getcwd()

    # Cambio la directorio de trabajo
    os.chdir( retpath + "/dirSalidas" )

"""
###############################################################################
Creación del archivo Shapefile

NOTA: Al final es necesario liberar la memoria mediante archivo.Destroy()
###############################################################################
"""
def CrearShp(filename):
    """Crea y devuelve archivo Shapefile vacío. Finalizar con OGR_DS_Destroy()
    """
#    slash = sintax_for_os()
#
#    #filename = str(raw_input("Nombre de archivo Shapefile a crear: "))
#    filename = "image_shape"
#
#    filename = str(directorio) + slash + filename + ".shp"

    estado = 0
    try:
        # Obtiene el driver
        driver = ogr.GetDriverByName("ESRI Shapefile")

        # Borra el archivo si existe previamente y luego lo crea
        if os.path.exists(filename):
            driver.DeleteDataSource(filename)

        """
        Parámetros opcionales de creación en http://www.gdal.org/drv_shapefile.html
        """
        archivo = driver.CreateDataSource(filename)
        if archivo is None:
            estado = 16
    except:
        estado = 16

    return [archivo, filename, estado]
# NOTA: Al final es necesario liberar la memoria mediante "archivo = None"

"""
###############################################################################
Creación de capa de puntos
###############################################################################
"""
def CrearCapaPuntos(archivo, lista_Ev, nombre_capa = 'focos', EPSG = 4326):
    """Crea capa de puntos en archivo Shapefile de entrada
    """
# DM: Se define un objeto clase llamado spatialRef, clase definida en osr.
# DM: Dicho objeto es configurado con la norma de referencia espacial EPSG.

    estado = 0
    # Defino la referencia espacial
    spatialRef = osr.SpatialReference()
    try:
        spatialRef.ImportFromEPSG(EPSG)
    except:
        EPSG = 4326
        spatialRef.ImportFromEPSG(EPSG)
        estado = 17

# DM: Se crea el objeto layer, que simboliza una capa del archivo shp.
# DM: Los atributos necesarios para la creacion de una capa son:
# DM: el nombre, la referencia espacial configurada, y referencia a atributos
# DM: correspondientes a "puntos".
    try:
        layer = archivo.CreateLayer(nombre_capa, spatialRef, ogr.wkbPoint)

    # DM: Luego de crear el layer, el mismo tiene la capacidad de definir campos
    # DM: de informacion respecto de cada uno de los elementos que se carguen,
    # DM: que en este caso seria puntos.

        # Añade un campo id al shapefile de salida
        fieldDefn = ogr.FieldDefn('id', ogr.OFTString)
        layer.CreateField(fieldDefn)
        # Obtiene el featureDefn para la capa de salida
        featureDefn = layer.GetLayerDefn()

    # DM: Se solicita la cantidad de puntos a añadir para reservar el espacio en
    # DM: memoria. Luego se solicita en pantalla que cargues los mismos, se

        for index_Ev in range(len(lista_Ev)):

            for index_Fo in range(lista_Ev[index_Ev].CantFocos()):

                point = ogr.Geometry(ogr.wkbPoint)
                point.AddPoint( float(lista_Ev[index_Ev].Foco[index_Fo].Lon()), \
                float(lista_Ev[index_Ev].Foco[index_Fo].Lat()) )






                # Crea un nuevo rasgo
                feature = ogr.Feature(featureDefn)
                feature.SetGeometry(point)
                string = lista_Ev[index_Ev].id

                feature.SetField('id', string)

                # Añade el rasgo a la capa de salida
                layer.CreateFeature(feature)

                # Destruye la geometría y el rasgo
                point.Destroy()
                feature.Destroy()

    except:
        estado = 16

    return estado

"""
###############################################################################
Inicio del programa de creacion de archivos shp
###############################################################################
"""
def Creater(lista_Ev, filename):

    file_Ev,file_shp,estado = CrearShp(filename)
    if(estado == 0):
        estado = CrearCapaPuntos(file_Ev, lista_Ev)
        try:
            file_Ev.Destroy()
        except:
            estado = 16

    return estado



"""
###############################################################################
Inicio del programa de comparacion de archivos shp
###############################################################################
"""

def Comparar_Entradas_Shp(lista_Ev,lista_prov):

    estado = 0
    prov_terminadas = []
#    print lista_prov
    try:
        for index_prov in lista_prov:

            if not os.path.exists(directorios_shp[index_prov]):
                #print "La provincia",index_prov, "no posee parques nacionales"
                prov_terminadas.append(index_prov)
                continue

            with fiona.open(directorios_shp[index_prov], 'r') as reservas_shp:
                #print  index_prov

                for index_Ev in range(len(lista_Ev)):
                    for index_Fo in range(lista_Ev[index_Ev].CantFocos()):

                        prov = lista_Ev[index_Ev].Foco[index_Fo].Prov()

                        if ( prov in prov_terminadas ) or ( index_prov != prov ):
                            continue

                        punto = lista_Ev[index_Ev].Foco[index_Fo].Punto_Shp()

                        for reserva in reservas_shp:
                            poligono = shape(reserva['geometry'])
                            if poligono.contains(punto):

                                if "ROTULO" in reserva['properties']:
                                    nombre_res = reserva['properties']['ROTULO']

                                elif "Name" in reserva["properties"]:
                                    nombre_res = reserva['properties']['Name']

                                elif "NOMBRE" in reserva["properties"]:
                                    nombre_res = reserva['properties']['NOMBRE']

                                else:
                                    nombre_res = "Reserva Natural: sin datos"

                                lista_Ev[index_Ev].Foco[index_Fo].Cargar_Reserva(nombre_res.encode('utf-8'))
                                lista_Ev[index_Ev].Cargar_Reserva(nombre_res.encode('utf-8'))

            prov_terminadas.append(index_Ev)
    except:
        estado = 12

    return [lista_Ev,estado]

def UTM(lat,lon,forceZoneNumber):
    return utm.from_latlon(lat,lon,forceZoneNumber)

def Adjuntar_Localidad(lista_Ev):

    #  Se abre el archivo shp que contiene las localidades de Arg.
    ds = ogr.Open(dir_localidades)

    lista_localidades = []
    if ds != None :
        #  Obtine la capa de puntos
        #### que tipo de objeto es layer? Que contiene?
        layer = ds.GetLayer()

        for index in xrange(layer.GetFeatureCount()):
            feature = layer.GetFeature(index)
            geometry = feature.GetGeometryRef()
            name = feature.GetFieldAsString('LOCALIDAD')
            lista_localidades.append([geometry.GetX(),geometry.GetY(),name])

        for index_Ev in range(len(lista_Ev)):
            for index_Fo in range(lista_Ev[index_Ev].CantFocos()):
                eje_xf, eje_yf, numf, keyf = lista_Ev[index_Ev].Foco[index_Fo].UTM()

                dist_min = 999999
                localidad_cerca = ""
                for index_Lo in range(len(lista_localidades)):
                    eje_xc, eje_yc, numc, keyc = UTM(lista_localidades[index_Lo][1],lista_localidades[index_Lo][0],numf)

                    dist_x,dist_y = eje_xf-eje_xc , eje_yf-eje_yc
                    dist = pow( pow(dist_x,2) + pow(dist_y,2) , 0.5 )
                    #print dist,dist_x,dist_y,dist_min
                    if dist < dist_min:
                        dist_min = dist
                        localidad_cerca = lista_localidades[index_Lo][2]
                        #print index_Lo,lista_localidades[index_Lo][2], dist_min

                if (localidad_cerca != ""):
                    lista_Ev[index_Ev].Foco[index_Fo].Cargar_Localidad(localidad_cerca)
                    lista_Ev[index_Ev].Cargar_Localidad(localidad_cerca)
    else:
        print "No se pudo abrir el directorio", dir_localidades
