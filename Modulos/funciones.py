#!/usr/bin/env python
#-*- coding: utf-8

"""
Nombre de script:    functions.py
Objetivo:            Funciones de uso general
Autor:               Daniel
"""
import os, time
from Clases.foco_y_evento     import Foco
from Clases.familia_raster    import *
from Modulos.manejo_shp       import Creater
from Modulos.definiciones     import *

###############################################################################
def Norma(lista_in , provincias_filtradas , fecha_ini, fecha_fin, op = 1 , modo = "" ):
    if op == 0:
        return lista_in
    lista_out = []
    provincias_analizadas = []
    estado = 0
    if op == 4:

        if (modo == "sepa"):
            for i in range(len(lista_in)):

                lugar = lista_in[i][2].lstrip('_').replace(',_',',')\
                .replace('_',' ').split(',')
                try:
                    FRP = float(lista_in[i][5])
                except:
                    FRP = 999

                try:
                    for i_prov in provincias_filtradas:
                        if  (lugar[0] == 'Argentina') and \
                            (lugar[1] in provincias_keys[i_prov]) and \
                            (lista_in[i][6] >= fecha_ini) and\
                            (lista_in[i][6] <= fecha_fin):
                            lista_out.append(Foco(lista_in[i][0], lista_in[i][1], \
                                             lista_in[i][6], lista_in[i][8], \
                                             lugar[0],provincias_keys[i_prov][0],lugar[2],FRP))
                            provincias_analizadas.append(provincias_keys[i_prov][0])
                            break

                except:
                    print u"Error de función Norma en línea:", i
                    print lista_in[i][0], lista_in[i][1], lugar
                    estado = 9
                    pass

        elif( modo == "modis" or modo == "modis shp"):
            for i in range(len(lista_in)):

                try:
                    FRP = float(lista_in[i][7])
                except:
                    FRP = 999

                try:
                    for i_prov in provincias_filtradas:
                        if  (lista_in[i][2] == 'Argentina') and \
                            (lista_in[i][3] in provincias_keys[i_prov]) and \
                            (lista_in[i][5] >= fecha_ini) and\
                            (lista_in[i][5] <= fecha_fin):
                            lista_out.append(Foco(lista_in[i][0], lista_in[i][1], \
                                             lista_in[i][5], lista_in[i][6], \
                                             lista_in[i][2],provincias_keys[i_prov][0],\
                                             lista_in[i][4],FRP))
                            provincias_analizadas.append(provincias_keys[i_prov][0])
                            break

                except:
                    print u"Error de función Norma en línea:", i
                    print lista_in[i][0], lista_in[i][1], lugar
                    estado = 9
                    pass
        else:
            estado = 8

        provincias_analizadas = valoresUnicos2(provincias_analizadas)

        return lista_out , provincias_analizadas , estado

    for i in range(len(lista_in)):
        lista_out.append([float(lista_in[i][0]),float(lista_in[i][1])])
        FechaHora = lista_in[i][6]+lista_in[i][8]

        if op == 2:
            lista_out[i].append(int(FechaHora))

        if op == 3:
            lista_out[i].append(int(FechaHora[0:4]))
            for j in [0,1,2,3]:
                lista_out[i].append(int(FechaHora[4+j*2:6+j*2]))

    return lista_out

###############################################################################
def valoresUnicos(lista_in):
    N = len(lista_in)
    lista_out = []
    for i in range(N-1,-1,-1):
        if (lista_in[i] not in lista_out):
            lista_out.append(lista_in[i])
    lista_out.sort(reverse=True)
    return lista_out

###############################################################################

def valoresUnicos2(lista_in):
    lista_out = []
    [ lista_out.append(lista_in[x]) for x in range(len(lista_in)-1,-1,-1) if \
    lista_in[x] not in lista_out ]
    return lista_out


###############################################################################
def ToFile(lista_Ev,nuevo_directorio = "no data", nombre_proyecto = "no data",\
           guardar_seleccion = [], objetos_raster = []):

    estados = []
    estado = 0
    slash = sintax_for_os()
    if(nuevo_directorio == "no data"):
        working_directory = str(os.getcwd())
    else:
        working_directory = str(nuevo_directorio)

    tipos_guardar_total = tipos_guardar + tipos_guardar_especial

###############################################################################
# Creacion de un nuevo directorio.
# formato: /[ eventos ]/"fecha_del_proceso"/round_xxx/
    try:
        i=0
        while True:
            if(nombre_proyecto == "no data"):
                folder = working_directory + slash + '[ eventos ]' + slash + \
                time.strftime("%y%m%d") + slash + 'round_' + str(i).zfill(3) + slash
            else:
                folder = working_directory + slash + str(nombre_proyecto) + slash + \
                time.strftime("%y%m%d") + slash + 'round_' + str(i).zfill(3) + slash

            if not os.path.exists(folder):
                os.makedirs(folder)
                break
            i += 1
    except:
        estado = 14

    try:
        if (tipos_guardar_total[0] in guardar_seleccion):
###############################################################################
# formato: /[ eventos ]/"fecha_del_proceso"/round_xxx/eventos.txt
            path_file1 = folder + eventos_file + '.txt'
            file_name1 = os.path.expanduser(path_file1)

            file_eventos = open(file_name1, 'w+')
            file_eventos.write(title_files_dict[eventos_file])

###############################################################################
# formato: /[ eventos ]/"fecha_del_proceso"/round_xxx/evento_xxx/
            cant_eventos = len(lista_Ev)
            for i in range(cant_eventos):

                cant_focos = lista_Ev[i].CantFocos()
                folder2 = folder + 'evento_' + str(lista_Ev[i].id).zfill(3) + slash
                os.makedirs(folder2)
###############################################################################
# Se prepara en formato acorde el listado de los eventos existentes.

                file_eventos.write( str(lista_Ev[i]) )

###############################################################################
# formato: /[ eventos ]/"fecha_del_proceso"/round_xxx/evento_xxx/focos.txt
                path_file2 = folder2 + focos_file + '.txt'
                file_name2 = os.path.expanduser(path_file2)

###############################################################################
# creacion y apertura del archivo de eventos.
                file_focos = open(file_name2, 'w+')
                file_focos.write(title_files_dict[focos_file])

                for j in range(cant_focos):
                    if j <= lista_Ev[i].IDIni():
                        text = lista_Ev[i].Foco[j].__str__("Inicial")
                    elif j >= lista_Ev[i].IDFin():
                        text = lista_Ev[i].Foco[j].__str__("Final")
                    else:
                        text = lista_Ev[i].Foco[j].__str__("Intermedio")
                    file_focos.write( text )

            file_eventos.close()
            file_focos.close()

    except:
        estado = 15
    estados.append(estado)



    estado = 0
    if (tipos_guardar_total[1] in guardar_seleccion):
        # Se crean los Shapefiles
        slash = sintax_for_os()
        filename = "image_shape"
        filename = str(folder) + slash + filename + ".shp"

        estado = Creater(lista_Ev,filename)
    estados.append(estado)

    estado = 0

    if (tipos_guardar_total[2] in guardar_seleccion):

        for index in range(len(objetos_raster)):
            folder_Ev = folder + slash + 'evento_' + \
                            str(objetos_raster[index].id) + slash

            if not os.path.exists(folder_Ev):
                os.makedirs(folder_Ev)

            if( (objetos_raster[index].info) == "NDVI"):
                objetos_raster[index].Adjuntar_Riesgo()
                objetos_raster[index].GuardarDatos(folder_Ev)
                pass

            if( (objetos_raster[index].info) == "LST"):
                objetos_raster[index].GuardarDatos(folder_Ev)
                pass

            if( (objetos_raster[index].info) == "GLC2000"):
                objetos_raster[index].Adjuntar_Traduccion()
                objetos_raster[index].GuardarDatos(folder_Ev)
                pass

    estados.append(estado)

    return estados
