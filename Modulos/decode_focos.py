#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Nombre de script:    decode_focos.py
Objetivo:            Decodificar archivos de entrada de focos
Autor:               Daniel
"""
import fiona, json
from shapely.geometry           import shape, Point
from operator                   import itemgetter
from urllib2                    import urlopen
from Modulos.definiciones       import *


## Se definen variables globales para analizar el estado actual
estado_total = 1
estado_parcial = 0

###############################################################################
#   Función de consulta de datos a traves de una api de Google
###############################################################################

def getplace(lat, lon):
    estado = 0
    url = "http://maps.googleapis.com/maps/api/geocode/json?"
    url += "latlng=%s,%s&sensor=false" % (lat, lon)
    try:
        v = urlopen(url).read()
        j = json.loads(v)
        components = j['results'][0]['address_components']
    except:
        estado = 23
    route = None
    depto = None

    if estado == 0:
        for c in components:
    #        if "country" in c['types']:
    #            country = c['long_name']
    #        if "postal_town" in c['types']:
    #            town = c['long_name']
    #        elif "administrative_area_level_1" in c['types']:
    #            town = c['long_name']
            if "administrative_area_level_2" in c['types']:
                depto = c['long_name']
            if "route" in c['types']:
                route = c['short_name']
                if route == "Unnamed Road":
                    route = c['long_name']

        if route == "Unnamed Road":
            route = None
        if depto == "Unnamed Road":
            depto = None
    return [route,depto,estado]


###############################################################################
#   Inicio de declaracion de funciones del programa
###############################################################################

def ModoSepa(fname):
    """ Función para decodificar archivo txt con formato "SEPA
    """
    global estado_total, estado_parcial
    lista_focos = []
    estado = 0
    #Proceso de lectura y migracion a una lista formato "SEPA"

    archivo = open(fname, "r")

    lista = str(archivo.read()).replace(";;","; ;").replace(" ","_")\
            .replace(";"," ").split()

    if(lista[:9] == formato_sepa):

        #Proceso de creacion de una tupla de focos actuales
        #longitud = len(lista)
        try:
            cant_focos = len(lista)/9-1

            for index in range(1,cant_focos+1):
                lista_focos.append(lista[9*index:9*(index+1)])
        except:
            estado = 4
            lista_focos = []
    else:
        estado = 3


    return [lista_focos,estado]


def ModoModis(fname):
    """Funcion para decodificar archivo csv con formato "MODIS"
    """
    global estado_total, estado_parcial
    print("Modo de carga MODIS.")
    estado = 0
    lista_focos = []
    #Proceso de lectura y migracion a una lista formato "MODIS"
    archivo = open(fname, "r")
    lista = str(archivo.read()).replace("\n",",").split(",")
    lista.pop()
    if(lista[:13] == formato_modis):

        #Proceso de creacion de una tupla de focos actuales
        #longitud = len(lista)
        cant_focos = len(lista)/13 -1

        try:
            for index in range(1,cant_focos+1):
                lista_aux = lista[13*index:13*(index+1)]
                lat = float(lista_aux[0])
                if (lat > -21.75):
                    continue
                lon = float(lista_aux[1])
                if (lon > -53.75):
                    continue

                lista_focos.append(lista_aux)

            lista_focos_sort = sorted(lista_focos, key=itemgetter(0),reverse=True)
        except:
            estado = 4
            lista_focos = []

        if(estado == 0):
            lista_focos = []
            try:
                with fiona.open(dir_provincias, 'r') as prov_shp:
                    prov = range(len(prov_shp))
                    prov_mas_limite = []
                    for i in prov:
                        minx, miny, maxx, maxy = shape(prov_shp[i]['geometry']).bounds
                        prov_mas_limite.append([maxy,prov_shp[i]])


                    prov_sort = sorted(prov_mas_limite, key=itemgetter(0),reverse=True)

                    prov_shp_sort = [prov_sort[i][1] for i in range(len(prov_sort)) ]

            #        for i in range(len(prov_sort)):
            #            print prov_shp_sort[i]['properties'][u'NOMBRE'].encode('utf-8')

                    for i in range(len(lista_focos_sort)):
                        lat = float(lista_focos_sort[i][0])
                        lon = float(lista_focos_sort[i][1])
                        punto = Point(float(lista_focos_sort[i][1]),float( lista_focos_sort[i][0]) )
            #            if shape(paises_shp[0]['geometry']).contains(punto):
            #                continue
                        fecha = lista_focos_sort[i][5].replace("-","")
                        hora = lista_focos_sort[i][6]
                        FRP = lista_focos_sort[i][11]
                        depto = "No hay dato"
                        pais = u"Argentina"


                        for j in prov:
                            poligono = shape(prov_shp_sort[j]['geometry'])
                            if poligono.contains(punto):
                                #print prov_shp_sort[j]['properties'][u'NOMBRE'].encode('utf-8')
                                provincia = prov_shp_sort[j]['properties'][u'NOMBRE']
                                prov.remove(j)
                                prov = [j] + prov
                                lista_focos.append([lat,lon,pais,provincia,depto,fecha,hora,FRP])
                                break
            except:
                estado = 5
                lista_focos = []

        #    for index in range(1,cant_focos+1):
        #        lista_aux = lista[13*index:13*(index+1)]
        #        lat = float(lista_aux[0])
        #        if (lat > -21.75):
        #            continue
        #        lon = float(lista_aux[1])
        #        if (lon > -53.75):
        #            continue
        #
        #        try:
        #            provincia, pais = getplace(lat,lon)
        #        except:
        #            print "falla timeout, conexion o limite de pedidos"
        #            continue
        #
        #        if (pais != u"Argentina"):
        #            continue
        #        if (provincia not in provincias_keys):
        #            continue
        #        provincia = provincias_keys[provincia][0]
        #        fecha = lista_aux[5].replace("-","")
        #        hora = lista_aux[6]
        #        FRP = lista_aux[11]
        #        depto = "No hay dato"
        #
        #        lista_focos.append([lat,lon,pais,provincia,depto,fecha,hora,FRP])
        #        print lista_focos[len(lista_focos)-1]

        #latitude,longitude,brightness,scan,track,acq_date,acq_time,satellite,confidence,version,bright_t31,frp,daynight
    else:
        estado = 3

    return [lista_focos,estado]

def ModoModisShp(fname):
    global estado_total, estado_parcial
    lista_focos = []
    estado = 0
#------------------------------------------------------------------------------
#  Proceso de adqusicion de los focos detectados.

#    with fiona.open(fname,'r') as focos_shp:
#        for foco in focos_shp:
#            #print focos_shp.schema
#            lat = float(foco["properties"]["LATITUDE"])
#            if (lat > -21.75):
#                continue
#            lon = float(foco["properties"]["LONGITUDE"])
#            if (lon > -53.75):
#                continue
#            pais = u"Argentina"
#            fecha = str(foco["properties"][u"ACQ_DATE"]).replace("-","")
#            hora = str(foco["properties"][u"ACQ_TIME"])
#            FRP = str(foco["properties"][u"FRP"])
#            depto = "No hay dato"
#            provincia = "NO DATA"
#
#            lista_focos.append([lat,lon,pais,provincia,depto,fecha,hora,FRP])
    try:
        with fiona.open(fname,'r') as focos_shp:
            for foco in focos_shp:
                #print focos_shp.schema
                lat = float(foco["properties"]["LATITUDE"])
                if (lat > -21.75):
                    continue
                lon = float(foco["properties"]["LONGITUDE"])
                if (lon > -53.75):
                    continue
                pais = u"Argentina"
                fecha = str(foco["properties"][u"ACQ_DATE"]).replace("-","")
                hora = str(foco["properties"][u"ACQ_TIME"])
                FRP = str(foco["properties"][u"FRP"])
                depto = "No hay dato"
                provincia = "NO DATA"

                lista_focos.append([lat,lon,pais,provincia,depto,fecha,hora,FRP])
    except:
        estado = 6
        lista_focos = []

#------------------------------------------------------------------------------
#  Proceso de ordenamiento de los focos detectados segun la latitud.
    if(estado == 0):
        lista_focos_sort = sorted(lista_focos, key=itemgetter(0),reverse=True)
        estado_total =  len(lista_focos_sort)
        lista_focos = []

        try:
            with fiona.open(dir_provincias, 'r') as prov_shp:
                prov = range(len(prov_shp))
                prov_mas_limite = []

        #------------------------------------------------------------------------------
        #  Proceso de ordenamiento de los poligonos provincias segun la latitud.

                for i in prov:
                    minx, miny, maxx, maxy = shape(prov_shp[i]['geometry']).bounds
                    prov_mas_limite.append([maxy,prov_shp[i]])

                prov_sort = sorted(prov_mas_limite, key=itemgetter(0),reverse=True)

                prov_shp_sort = [prov_sort[i][1] for i in range(len(prov_sort)) ]

        #------------------------------------------------------------------------------
        #  Proceso de detectar a que previncia pertenece cada foco detectado.

                for i in range(len(lista_focos_sort)):
                    estado_parcial = i+1
                    lat = float(lista_focos_sort[i][0])
                    lon = float(lista_focos_sort[i][1])
                    punto = Point(float(lista_focos_sort[i][1]),float( lista_focos_sort[i][0]) )

                    for j in prov:
                        poligono = shape(prov_shp_sort[j]['geometry'])
                        if poligono.contains(punto):
                            #print prov_shp_sort[j]['properties'][u'NOMBRE'].encode('utf-8')
                            provincia = prov_shp_sort[j]['properties'][u'NOMBRE']
                            prov.remove(j)
                            prov = [j] + prov
                            lista_focos_sort[i][3] = provincia
                            lista_focos.append(lista_focos_sort[i])
                            #print lista_focos[len(lista_focos)-1]
                            break
        except:
            estado = 5
            lista_focos = []

#    with fiona.open(fname,'r') as focos_shp:
#        for foco in focos_shp:
#            #print focos_shp.schema
#            lat = float(foco["properties"]["LATITUDE"])
#            if (lat > -21.75):
#                continue
#            lon = float(foco["properties"]["LONGITUDE"])
#            if (lon > -53.75):
#                continue
#            try:
#                provincia, pais = getplace(lat,lon)
#            except:
#                print "falla timeout"
#            if (pais != u"Argentina"):
#                continue
#            if (provincia not in provincias_keys):
#                continue
#            provincia = provincias_keys[provincia][0]
#            fecha = str(foco["properties"][u"ACQ_DATE"]).replace("-","")
#            hora = str(foco["properties"][u"ACQ_TIME"])
#            FRP = str(foco["properties"][u"FRP"])
#            depto = "No hay dato"
#
#            print [lat,lon,pais,provincia,depto,fecha,hora,FRP]
#            lista_focos.append([lat,lon,pais,provincia,depto,fecha,hora,FRP])

    return [lista_focos,estado]


###############################################################################
#   Inicio del programa de decodificacion
###############################################################################
def Decoder(fname, tipo_mod14):

#    modo = raw_input("Ingrese el tipo de archivo a decodificar(s:SEPA m:MODIS\
#): ")
    if(tipo_mod14 in modoDeCarga_dic):
        #ClearScreen()
        listas = modoDeCarga_dic[tipo_mod14](fname)
        #ClearScreen()
    else:
        #ClearScreen()
        print "Modo incorrecto. Intente de nuevo."
    return listas

def Decoder_Estado():
    global estado_total, estado_parcial
    return [estado_total, estado_parcial]



def Adjuntar_Rutas_Deptos(lista_Ev):

    estados = []
    for index_Ev in range(len(lista_Ev)):
        for index_Fo in range(lista_Ev[index_Ev].CantFocos()):
            lat = lista_Ev[index_Ev].Foco[index_Fo].Lat()
            lon = lista_Ev[index_Ev].Foco[index_Fo].Lon()
            [ruta,depto,estado] = getplace(lat,lon)


            if (estado == 0):
                if(ruta != None):
                    lista_Ev[index_Ev].Foco[index_Fo].Cargar_Rutas(ruta.encode('utf-8'))
                    lista_Ev[index_Ev].Cargar_Rutas(ruta.encode('utf-8'))
                if(depto != None):
                    lista_Ev[index_Ev].Foco[index_Fo].Cargar_Depto(depto.encode('utf-8'))
                    #lista_Ev[index_Ev].Cargar_Depto(ruta.encode('utf-8'))

            elif(estado not in estados):
               estados.append(estado)

    return [lista_Ev,estados]
###############################################################################
#   Inicio de declaracion de variables, listas, strings y diccionarios
###############################################################################

modoDeCarga_dic = {"sepa":ModoSepa,\
                   "modis":ModoModis,\
                   "modis shp":ModoModisShp}

OpSys = 'linux'

provincias_keys = {'Jujuy':['Jujuy'],\
                   'Salta':['Salta'],\
                  'Santiago del Estero':['Santiago del Estero'],\
                  u'Tucumán':['Tucuman','Tucumn'],\
                  'La Rioja':['La Rioja'],\
                  'Catamarca':['Catamarca'],\
                  'San Luis':['San Luis'],\
                  'Buenos Aires':['Buenos Aires'],\
                  'Santa Fe':['Santa Fe'],\
                  u'Córdoba':['Cordoba'],\
                  'Terra del Fuego':['Terra del Fuego'],\
                  'Misiones':['Misiones'],\
                  u'Entre Ríos':['Entre Rios','Entre Ros'],\
                  'Mendoza':['Mendoza'],\
                  'Chaco':['Chaco'],\
                  'San Juan':['San Juan'],\
                  'La Pampa':['La Pampa'],\
                  'Santa Cruz':['Santa Cruz'],\
                  'Chubut':['Chubut'],\
                  u'Neuquén':['Neuquen'],\
                  u'Río Negro':['Rio Negro','Ro Negro'],\
                  'Corrientes':['Corrientes'],\
                  'Formosa':['Formosa']}


###############################################################################
# Código de prueba
###############################################################################
if __name__ == '__main__':
    Decoder()

