#!/usr/bin/env python
#-*- coding: utf-8
"""
Nombre de script:    detector_eventos.py
Objetivo:            Detecta los eventos de incendio
Autor:               Iván - Daniel
"""

# Importa modulos y establece el directorio de trabajo
from datetime import date

"""
###############################################################################
Detecta los eventos de incendio
###############################################################################
"""

def DetEventos(lista_focos, v_espacial, v_temporal):

    """Detecta los eventos de incendio de una lista de focos. Devuelve la
    lista de eventos

    lista_focos: lista de focos a analizar

    v_espacial: distancia máxima entre focos de un mismo evento, en metros

    v_temporal: tiempo máximo entre focos de un mismo evento, en días
    """

    can_focos = len(lista_focos)
    num_focos = range(can_focos)
    aux = []
    eventos = []
    velocidad_evento = []
    vel_maxima = 0

    while num_focos != []:
        evento_focos = []
        evento_revisados = []
        # Agrega un foco al evento actual y lo quita de la lista de los
        # focos pendientes (numFocos)
        evento_focos.append(num_focos.pop(0))
        evento_revisados.append(False)

        cambio = True
        while cambio == True :
            cambio = False

            N = len(evento_focos)
            M = len(num_focos)

            if M == 0:
                break
            # Para todos los focos del evento actual ...
            for i in range(N):
                if evento_revisados[i] == True :
                    continue

                # foco = [lat, lon, año, mes, dia, hora, min]
                # Entonces obtengo los datos de la fecha
                D0, M0, Y0 = lista_focos[evento_focos[i]].Fecha()


                # Para los focos pendientes ...
                for j in range(M):
                    # Consulto la fecha del foco (pendiente)
                    # [foco pendiente denominado 1]
                    D1, M1, Y1 = lista_focos[num_focos[j]].Fecha()

                    # Crea un calendario para cada foco, con
                    # su fecha correspondiente
                    F0 = date(Y0, M0, D0)
                    F1 = date(Y1, M1, D1)



                    # Si el tiempo entre ambos fue mayor que la
                    # ventana temporal + 1 día, termina el análisis
                    t = F1 - F0


                    # TODO : Probar que pasa usando t = abs (F1 - F0)
                    # Tarda menos??? falla el algoritmo??
                    if t.days > (v_temporal + 1) :
                        break

                    # Si el tiempo entre ambos fue menor o igual que
                    # la ventana temporal ...
                    if abs(t.days) <= v_temporal :
                        # TODO Hablar con Alfredo sobre la utilización de medidas que no dependan de la distancia al Ecuador

                        # Mide la distancia EN LATITUD Y LONGITUD (A CORREGIR !!) entre el foco actual y el pendiente
                        #delta_y = lista_focos[evento_focos[i]].Lat() - lista_focos[num_focos[j]].Lat()
                        #delta_x = lista_focos[evento_focos[i]].Lon() - lista_focos[num_focos[j]].Lon()
                        # Si la distancia medida es menor que la distancia límite "dmin" ...
                        # TODO NOTA @Ivan: Yo pondría "menor o igual" a la distancia límite, y la llamaría dmax

                        # Utilizo la libreria utm para trabajar directamente con la proyeccion en metros.
                        # Utilizo el mismo numero de zona para evitar saltos de distancias.
                        #"""
                        eje_x1, eje_y1, num1, key1 = lista_focos[evento_focos[i]].UTM()
                        eje_x2, eje_y2, num2, key2 = lista_focos[num_focos[j]].UTM(num1)

                        delta_y = eje_y1 - eje_y2
                        delta_x = eje_x1 - eje_x2
                        #"""
                        dist = pow(delta_x,2) + pow(delta_y,2)
                        if  pow(v_espacial,2) > (  dist  ) :
                            # Agrega el foco a un evento auxiliar y activa flag para iniciar nuevo procesamiento
                            cambio = True
                            aux.append(num_focos[j])

                            if (t.days > 0):
                                vel_actual = int(pow(dist,0.5)/t.days)
                                if vel_maxima < vel_actual:
                                    vel_maxima = vel_actual





                        #  TODO NOTA: Queda por analizar los que se pasan de la ventana temporal por menos de un día !!

                    # Lo repite para todos los focos pendientes

                # Hace el procesamiento para cada uno de los focos del evento actual, sin repetir lo ya procesados
                evento_revisados[i] = True;
            # Si hay algún foco en el evento auxiliar...
            if aux :
                # Crea una lista de focos sin focos repetidos, ordenados de mayor a menor

                # TODO ver si hace falta luego que Daniel haya programado la parte de abajo
                aux2 = ValoresUnicos(aux) # La salida de valores unicos tienen que estar ordenados de mayor a menor
                P = len(aux2)
                # (Empieza el análisis desde los focos mayores)
                for k in range(P) :
                    # Crea un nuevo evento sin focos repetidos, evento que hay que revisar
                    # Es necesario revisarlo por tener nuevos focos que pueden estar cerca de focos limítrofes al evento
                    evento_focos.append(aux2[k]) # eventos(numEvento).focos.add( aux2(k) );
                    evento_revisados.append(False)

                    # Quita de los focos pendientes, el foco agregado al evento
                    #num_foco = num_focos.index(   );
                    num_focos.remove(aux2[k]);

                    # TODO  El siguiente código debería de hacer lo mismo de forma optimizada
                    """
                    # Quita el foco de la lista de los "focos pendientes" y lo
                    # lo agrega al evento. Evento sin focos repetidos
                    lugar = num_focos.index( aux2[k] )
                    evento_focos.append( num_focos.pop(lugar) )
                    # Es necesario revisarlo por tener nuevos focos que pueden estar cerca de focos limítrofes al evento
                    evento_revisados.append(False)
                    """

                aux  = []
                aux2 = []

        eventos.append(evento_focos)
        velocidad_evento.append(vel_maxima)
        vel_maxima = 0

    # Elimina los eventos con un solo foco
    Q = len(eventos)

    # lista_eventos es el valor de retorno
    lista_eventos = []
    lista_velocidades = []

    for i in range(Q) :
        # Si el evento tiene más de un foco,
        auxEvento = eventos[i]
    ##  Se modifica para que la cantidad de focos minimo de un evento sean de 3
    ##  para considerarlo como tal. De esta manera se podrá calcularlo una
    ##  superficie.
        #if len(auxEvento) > 1 :
        if len(auxEvento) > 2 :
            # crea una lista de sus focos
            focos_evento = [lista_focos[x] for x in auxEvento]
            # Agrego el evento actual (lista de clase Focos) a la lista de eventos
            lista_eventos.append(focos_evento)

            lista_velocidades.append(velocidad_evento[i])

    # TODO Ver si se mantiene el cant_eventos o si se quita
    cant_eventos = len(lista_eventos)

    return [lista_eventos, cant_eventos, lista_velocidades]

"""
###############################################################################
Genera una lista sin focos repetidos, ordenados de mayor a menor

Parámetro de entrada: Lista de focos. Retorna: Lista de focos sin repetir
###############################################################################
"""
def ValoresUnicos(lista_in):
    """Genera una lista sin focos repetidos, ordenados de mayor a menor
    """
    N = len(lista_in)
    lista_out = []
    for i in range(N):
        if (lista_in[i] not in lista_out):
            lista_out.append(lista_in[i])
    lista_out.sort(reverse=True)
    return lista_out

"""
###############################################################################
Inicio del programa de creación de archivos Shapefile
###############################################################################
"""

"""
def Creater(lista_focos):
    DefDirectorio()
    file_focos = CrearShp()
    CrearCapaPuntos(file_focos, lista_focos)
    file_focos.Destroy()
"""