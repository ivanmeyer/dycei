#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Nombre de script:    _gdal_data.py
Objetivo:            Verificación de la variable de entorno GDAL_DATA
Autor:               Iván
"""

"""
TODO(alguien): reemplazar manejo de directorios por lo explicado en:
https://docs.python.org/2/library/os.path.html

Es necesario ese método para que sea código multiplataforma.

"""

import os
import subprocess
import sys

#%%
def VerificarGDAL_DATA():
    """Consulta la configuración de la variable de entorno GDAL_DATA
    """

    # Consulto si la variable se detectó al iniciar Python
    if 'GDAL_DATA' in os.environ:
        # La variable se detectó al iniciar Python
        print "La variable de entorno GDAL_DATA está configurada."
    else:
        # La variable no se detectó al iniciar Python
        print "Variable de entorno GDAL_DATA no configurada."

#%%
def ConfigurarGDAL_DATA():
    """Configura la variable de entorno GDAL_DATA
    """

    """
    TODO: Programar para todos los SO!

    Leer archivo "gdal_data info.odt" (consultar a Iván)
    """

    if 'GDAL_DATA' in os.environ:
        print "Variable de entorno GDAL_DATA ya configurada."
    else:

        try:
            # el comando "gdal-config --datadir" devuelve la ruta de GDAL_DATA
            path = subprocess.check_output(["gdal-config", "--datadir"]).rstrip()
        except OSError:
            print "El programa «gdal-config» no está instalado."
            print "Debe instalarlo para poder ejecutar esta función"
            sys.exit("Instale 'gdal-config'")

        else:
            # Configuración de la variable de entorno
            _set_permanently(path)


def _set_permanently(path):
    """Configura en forma permanente la variable de entorno GDAL_DATA
    """

    #=========================================================================
    """Se intenta agregar las siguientes lineas al final del setup del bash"""
    #
    # # added by dycei
    # export GDAL_DATA=/home/NOMBRE_USUARIO/anaconda2/share/gdal
    #
    #

    """   el directorio correcto se obtuvo en '_check_gdal_data()'

    Todo esto es equivalente a:
            os.system('bash -c \'echo "export MENSAJITO" >> ~/.bashrc\'')
    """
    #=========================================================================

    try:
        # Escribo lineas en el setup del bash
        retcode = subprocess.call('bash -c \'printf "\n# added by dycei\
        \nexport GDAL_DATA=' + path + '\n\n" >> ~/.bashrc\'', \
        shell=True)
    except OSError as e:
        # En caso de excepción, lo indico en pantalla
        print >>sys.stderr, "Falló la ejecución:", e
    else:
        # Caso exitoso, lo indico en pantalla y pido reiniciar el bash
        if retcode == 0:
            print "Se configuró la variable de entorno GDAL_DATA. "\
    "Por favor reinicie la sesión ! "


    """
    TODO: Si las funciones de print no andan podemos usar la que está a
    continuacion:

            print >>sys.stderr, "Valor devuelto:", retcode

    """

#def PruebaGDAL():
#    from osgeo import osr
#
#    # Defino la referencia espacial
#    spatialRef = osr.SpatialReference()
#    spatialRef.ImportFromEPSG(4326)
#
#    print "Todo OK"

if __name__ == '__main__':
    ConfigurarGDAL_DATA()
