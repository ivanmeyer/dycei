#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Nombre de script:    _check_gdal.py
Objetivo:            Verificación de instalación de GDAL
Autor:               Iván
http://pcjericks.github.io/py-gdalogr-cookbook/gdal_general.html
"""

import sys

#%%
def EstaGdal():
    """Consulta si GDAL/OGR se encuentra instalado y configurado
    """
    try:
        from osgeo import ogr, osr, gdal
    except ImportError as e:
        print "Módulos GDAL/OGR no encontrados."
        print "Intente:"
        print "     - verificar las variables de entorno"
        print "     - ejecutar <<conda install hdf4>>"
        print "El detalle del error es: ",e
        sys.exit('ERROR: No se puede encontrar los módulos GDAL/OGR')
    else:
        print "GDAL/OGR está instalada"

#%%
def VersionGdal():
    """Consulta la versión de GDAL/OGR instalada
    """    
    from osgeo import gdal
    
    version = int(gdal.VersionInfo('VERSION_NUM'))
    print "La versión de GDAL/OGR instalada es %s" %(gdal.__version__)
    if version < 1100000:
        sys.exit('ERROR: Son necesarios la vinculación (los bindings) de '\
                 'Python con GDAL 1.10 o posterior')

#%%
def HabilitarExcepciones():
    """Habilita las excepciones de GDAL
    """
    from osgeo import gdal
    
    # Enable GDAL/OGR exceptions
    gdal.UseExceptions()
    
    # try to open dataset that does not exist
    try:    
        gdal.Open('Exception_OK.tif')
        """ No confundir una Excepción con un Error (del tipo err_class)"""
    # results in python RuntimeError exception that
    # `Exception_OK.tif' does not exist in the file system
    except RuntimeError:
        print "Excepciones habilitadas"    


#%%
def UsarManejadorErrores():
    """Instala el manejador de errores de GDAL/OGR
    """
    try:
        from osgeo import gdal
    except:
        sys.exit('ERROR: No se puede encontrar los módulos GDAL/OGR')
    
    # example GDAL error handler function
    def gdal_error_handler(err_class, err_num, err_msg):
        errtype = {
                gdal.CE_None:'None',
                gdal.CE_Debug:'Debug',
                gdal.CE_Warning:'Warning',
                gdal.CE_Failure:'Failure',
                gdal.CE_Fatal:'Fatal'
        }
        err_msg = err_msg.replace('\n',' ')
        err_class = errtype.get(err_class, 'None')
        print 'Error número: %s' % (err_num)
        print 'Error tipo: %s' % (err_class)
        print 'Mensaje: %s' % (err_msg)
    
    # install error handler
    gdal.PushErrorHandler(gdal_error_handler)

    # Raise a dummy error
    gdal.Error(1, 2, 'Error de comprobación')
              
#%%
def NoUsarManejadorErrores():
    """Desinstala el manejador de errores de GDAL/OGR
    """
    try:
        from osgeo import gdal
    except:
        sys.exit('ERROR: cannot find GDAL/OGR modules')
    
    #uninstall error handler
    gdal.PopErrorHandler()
    
    print "Error Handler uninstalled"
    
#%%
def VerificarManejadorErrores():
    """Verifica la instalación del manejador de errores de GDAL/OGR
    """
    import gdal

    gdal.Error(0, 1, 'Manejador de errores instalado')


if __name__ == '__main__':
    print "Se realiza configuración automática"
    EstaGdal()
    VersionGdal()
    HabilitarExcepciones()
    UsarManejadorErrores()
    VerificarManejadorErrores()
    